﻿
namespace PortalForArbitrators.Options
{
    public class EmailSendOptions
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string SmtpHost { get; set; }
        public int Port { get; set; }
    }
}
