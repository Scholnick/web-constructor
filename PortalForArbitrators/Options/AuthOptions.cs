﻿
namespace PortalForArbitrators.Options
{
    public class AuthOptions
    {
        public string ClientID { get; set; }
        public string ClientSecret { get; set; }
        public string ReturnURL { get; set; }
        public string ProviderName { get; set; }
    }
}
