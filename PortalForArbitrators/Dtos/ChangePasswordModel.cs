﻿using PortalForArbitrators.Data.Entities;
using System.ComponentModel.DataAnnotations;

namespace PortalForArbitrators.Dtos
{
    public class ChangePasswordModel
    {
        [Required(ErrorMessage = "Введите, пожалуйста, старый пароль")]
        public string OldPassword { get; set; }
        [Required(ErrorMessage = "Введите, пожалуйста, новый пароль")]
        [StringLength(20, ErrorMessage = "Пароль должен содержать минимум 6 и максимум 20 символов", MinimumLength = 6)]
        public string NewPassword { get; set; }
        [Required(ErrorMessage = "Введите, пожалуйста, подтверждение нового пароля")]
        [Compare("NewPassword", ErrorMessage = "Пароли не совпадают")]
        public string ConfirmNewPassword { get; set; }
    }
}
