﻿using PortalForArbitrators.Data.Entities;

namespace PortalForArbitrators.Dtos
{
    public class ChangeUserInfoModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Image Avatar { get; set; }
    }
}
