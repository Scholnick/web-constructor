﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PortalForArbitrators.Migrations
{
    public partial class Change : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Url",
                table: "Domains");

            migrationBuilder.AddColumn<int>(
                name: "DomenType",
                table: "Domains",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Domains",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DomenType",
                table: "Domains");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Domains");

            migrationBuilder.AddColumn<string>(
                name: "Url",
                table: "Domains",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
