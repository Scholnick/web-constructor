﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PortalForArbitrators.Migrations
{
    public partial class DomainId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Domains_Sites_SiteId",
                table: "Domains");

            migrationBuilder.DropIndex(
                name: "IX_Domains_SiteId",
                table: "Domains");

            migrationBuilder.DropColumn(
                name: "SiteId",
                table: "Domains");

            migrationBuilder.AddColumn<Guid>(
                name: "DomainId",
                table: "Sites",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Sites_DomainId",
                table: "Sites",
                column: "DomainId");

            migrationBuilder.AddForeignKey(
                name: "FK_Sites_Domains_DomainId",
                table: "Sites",
                column: "DomainId",
                principalTable: "Domains",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Sites_Domains_DomainId",
                table: "Sites");

            migrationBuilder.DropIndex(
                name: "IX_Sites_DomainId",
                table: "Sites");

            migrationBuilder.DropColumn(
                name: "DomainId",
                table: "Sites");

            migrationBuilder.AddColumn<Guid>(
                name: "SiteId",
                table: "Domains",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Domains_SiteId",
                table: "Domains",
                column: "SiteId");

            migrationBuilder.AddForeignKey(
                name: "FK_Domains_Sites_SiteId",
                table: "Domains",
                column: "SiteId",
                principalTable: "Sites",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
