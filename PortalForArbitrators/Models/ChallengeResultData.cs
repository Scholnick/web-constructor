﻿using Microsoft.AspNetCore.Authentication;

namespace FastTrack_Platform_POC.Models
{
    public class ChallengeResultData
    {
        public string ProviderName { get; set; }
        public AuthenticationProperties AuthenticationProperties { get; set; }
    }
}
