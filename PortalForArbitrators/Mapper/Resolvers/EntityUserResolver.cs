﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using PortalForArbitrators.Data.Entities;
using PortalForArbitrators.Services.Interfaces;

namespace PortalForArbitrators.Mapper.Resolvers
{
    public class EntityUserResolver : IValueResolver<object, object, User>
    {
        private readonly IHttpContextAccessor _accessor;
        private readonly IIdentityService _identityService;

        public EntityUserResolver(IHttpContextAccessor accessor, IIdentityService identityService)
        {
            _accessor = accessor;
            _identityService = identityService;
        }

        public User Resolve(object source, object destination, User destMember, ResolutionContext context)
        {
            return _identityService.GetAppUserAsync(_accessor.HttpContext.User).Result;
        }
    }
}