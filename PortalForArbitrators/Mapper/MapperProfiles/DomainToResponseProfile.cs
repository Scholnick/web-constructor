﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using PortalForArbitrators.Contracts.v1.Responses;
using PortalForArbitrators.Data.Entities;

namespace PortalForArbitrators.Mapper.MapperProfiles
{
    public class DomainToResponseProfile : Profile
    {
        public DomainToResponseProfile()
        {
            CreateMap<Domain, DomainResponseList>();
            CreateMap<DomainResponseList, Domain>();
            CreateMap<Payment, PaymentResponse>();
            CreateMap<Site, SiteResponse>();
            CreateMap<Domain, DomainCreateRequest>();
            CreateMap<User, RegisterUserResponse>();
            CreateMap<SubscriptionPlan, SubscriptionPlanResponse>();
            CreateMap<Settings, Settings>();


            CreateMap<Template, TemplateCreateRequest>();
            CreateMap<TemplateCreateRequest, Template>();

            //CreateMap<Site, SiteInGroupsResponse>().AfterMap((site, siteInGroups) =>
            //{
            //    siteInGroups.DomainUrls = site.Domains.Select(domain => domain.Name).ToList();
            //});

            CreateMap<Subscription, SubscriptionResponse>().AfterMap((subscr, response, context) =>
            {
                response.SubscriptionPlan = context.Mapper.Map<SubscriptionPlan, SubscriptionPlanResponse>(subscr.SubscriptionPlan);
            });

            CreateMap<User, GetUserInfoResponse>().AfterMap((source, dest) =>
            {
                var avatar = source.Avatar;

                if (avatar != null)
                {
                    dest.ImgPath = avatar.FilePath;
                }
            });
        }
    }
}
