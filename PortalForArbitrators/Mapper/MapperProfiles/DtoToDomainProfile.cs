﻿using AutoMapper;
using PortalForArbitrators.Data.Entities;
using PortalForArbitrators.Dtos;

namespace PortalForArbitrators.Mapper.MapperProfiles
{
    public class DtoToDomainProfile : Profile
    {
        public DtoToDomainProfile()
        {
            CreateMap<ChangeUserInfoModel, User>();
        }
    }
}
