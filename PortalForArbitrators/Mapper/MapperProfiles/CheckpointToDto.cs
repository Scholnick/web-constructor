﻿using AutoMapper;
using PortalForArbitrators.Contracts.v1.Requests;
using PortalForArbitrators.Contracts.v1.Responses;
using PortalForArbitrators.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalForArbitrators.Mapper.MapperProfiles
{
    public class CheckpointToDto : Profile
    {
        public CheckpointToDto()
        {
            CreateMap<Settings, SettingsCreateRequest>();
            CreateMap<SettingsCreateRequest, Settings>();

            CreateMap<RobotsSettings, RobotsSettingsCreateRequest>();
            CreateMap<RobotsSettingsCreateRequest, RobotsSettings>();

            CreateMap<CookieSettings, CookieSettingsCreateRequest>();
            CreateMap<CookieSettingsCreateRequest, CookieSettings>();

            CreateMap<Page, PageCreateRequest>();
            CreateMap<PageCreateRequest, Page>();

            CreateMap<Site, SiteRuqest>();
            CreateMap<SiteRuqest, Site>();
            //.ForMember(dest => dest.Domains, act => act.Ignore());


            CreateMap<Geolocation, GeolocationRequest>();
            CreateMap<GeolocationRequest, Geolocation>();

            CreateMap<Geolocation, GeolocationResponse>();
            CreateMap<GeolocationResponse, Geolocation>();


            CreateMap<Checkpoint, CheckpointResponse>();
            CreateMap<CheckpointResponse, Checkpoint>();
            

        }
    }
}
