﻿using System;
using AutoMapper;
using PortalForArbitrators.Contracts.v1.Requests;
using PortalForArbitrators.Data.Entities;
using PortalForArbitrators.Mapper.Resolvers;

namespace PortalForArbitrators.Mapper.MapperProfiles
{
    public class RequestToDomainProfile : Profile
    {
        public RequestToDomainProfile()
        {
            CreateMap<RegisterUserRequest, User>()
                .BeforeMap((req, user) => {
                    user.TimeRegistered = DateTime.Now;
                    user.UserName = req.Email;
                });

            CreateMap<CreatePaymentRequest, Payment>().BeforeMap((req, payment) =>
            {
                if (req.Status == "")
                {
                    payment.Status = "Not Assigned";
                }

                payment.DateOfOperation = DateTime.Now;
            }).ForMember(payment => payment.User, options => {
                options.MapFrom<EntityUserResolver>();
            });

            CreateMap<SiteCreateRequest, Site>().BeforeMap((req, payment) =>
            {
                payment.TimeCreated = DateTime.Now;
            }).ForMember(payment => payment.User, options => {
                options.MapFrom<EntityUserResolver>();
            });
        }
    } 
}
