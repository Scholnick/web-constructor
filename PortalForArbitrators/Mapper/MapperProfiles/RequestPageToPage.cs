﻿using AutoMapper;
using PortalForArbitrators.Contracts.v1.Requests;
using PortalForArbitrators.Contracts.v1.Responses;
using PortalForArbitrators.Data.Entities;
using PortalForArbitrators.Dtos;
using PortalForArbitrators.Mapper.Resolvers;

namespace PortalForArbitrators.Mapper.MapperProfiles
{
    public class RequestPageToPage : Profile
    {
        public RequestPageToPage()
        {
            CreateMap<PageRequest, Page>();
            CreateMap<Page, PageResponse>();
        }
    }
}
