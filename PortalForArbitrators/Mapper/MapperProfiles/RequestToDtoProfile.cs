﻿using AutoMapper;
using PortalForArbitrators.Contracts.v1.Requests;
using PortalForArbitrators.Dtos;
using PortalForArbitrators.Mapper.Resolvers;

namespace PortalForArbitrators.Mapper.MapperProfiles
{
    public class RequestToDtoProfile : Profile
    {
        public RequestToDtoProfile()
        {
            CreateMap<ChangeUserProfileRequest, ChangeUserInfoModel>().ForMember(x => x.Avatar, opt => opt.Ignore());
            CreateMap<ChangeUserProfileRequest, ChangePasswordModel>();
        }
    }
}
