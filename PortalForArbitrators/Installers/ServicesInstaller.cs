﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PortalForArbitrators.Data.Entities;
using PortalForArbitrators.Repositories;
using PortalForArbitrators.Repositories.Interfaces;
using PortalForArbitrators.Services;
using PortalForArbitrators.Services.Interfaces;

namespace PortalForArbitrators.Installers
{
    public class ServicesInstaller : IInstaller
    {
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IIdentityService, IdentityService>();
            services.AddTransient<IEmailService, EmailService>();
            services.AddTransient<IUserService, UserService>();


            services.AddHttpContextAccessor();

            services.AddTransient<IGenericRepository<Payment>, BaseGenericRepository<Payment>>();
            services.AddTransient<IGenericRepository<Site>, BaseGenericRepository<Site>>();
            services.AddTransient<IGenericRepository<Domain>, BaseGenericRepository<Domain>>();
            services.AddTransient<IGenericRepository<SubDomain>, BaseGenericRepository<SubDomain>>();
            services.AddTransient<IGenericRepository<Subscription>, BaseGenericRepository<Subscription>>();
            services.AddTransient<IGenericRepository<SiteGroup>, BaseGenericRepository<SiteGroup>>();
            services.AddTransient<IGenericRepository<Image>, BaseGenericRepository<Image>>();
            services.AddTransient<IGenericRepository<Flag>, BaseGenericRepository<Flag>>();
            services.AddTransient<IGenericRepository<Geolocation>, BaseGenericRepository<Geolocation>>();
            services.AddTransient<IGenericRepository<Page>, BaseGenericRepository<Page>>();
            services.AddTransient<IGenericRepository<Constructor>, BaseGenericRepository<Constructor>>();
            services.AddTransient<IGenericRepository<Template>, BaseGenericRepository<Template>>();
            services.AddTransient<IGenericRepository<Settings>, BaseGenericRepository<Settings>>();
            services.AddTransient<IGenericRepository<Checkpoint>, BaseGenericRepository<Checkpoint>>();

            services.AddTransient<IGenericRepository<User>, BaseGenericRepository<User>>();

        }
    }
}
