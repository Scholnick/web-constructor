﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PortalForArbitrators.Data;

namespace PortalForArbitrators.Installers
{
    public class DbContextInstaller : IInstaller
    {
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            string connectionsString = configuration.GetConnectionString("SqlConnection");
            services.AddDbContext<ArbitratorsPortalDbContext>(options =>
                options.UseSqlServer(connectionsString));
        }
    }
}
