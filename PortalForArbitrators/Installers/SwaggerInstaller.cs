﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.IO;

namespace PortalForArbitrators.Installers
{
    public class SwaggerInstaller : IInstaller
    {
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo { Title = "PortalForArbitrators", Version = "v1" });

                //var xmlPath = System.AppDomain.CurrentDomain.BaseDirectory + @"IdentityServer.xml";
                //if (!File.Exists(xmlPath))
                //{
                //    using (File.Create(xmlPath)) { }

                //    string appendText = $"<?xml version=\"1.0\"?>" + Environment.NewLine;
                //    appendText += $"<doc>" + Environment.NewLine;
                //    appendText += $"</doc>";
                //    File.AppendAllText(xmlPath, appendText);
                //}
                //options.IncludeXmlComments(xmlPath);

                OpenApiSecurityScheme openApiSecurityScheme = new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer",
                    BearerFormat = "Bearer {token}",
                    Reference = new OpenApiReference
                    {
                        Type = ReferenceType.SecurityScheme,
                        Id = "Bearer"
                    },
                };

                options.AddSecurityDefinition("Bearer", openApiSecurityScheme);

                options.AddSecurityRequirement(new OpenApiSecurityRequirement() { { openApiSecurityScheme, new List<string>() } });
            });
        }
    }
}
