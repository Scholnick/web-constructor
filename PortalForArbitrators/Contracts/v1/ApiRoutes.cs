﻿namespace PortalForArbitrators.Contracts.v1
{
    public static class ApiRoutes
    {
        private const string Version = "v1";

        public static class Identity
        {
            public const string Base = "identity";
            public const string BaseWithVersion = Version + "/" + Base;
            public const string ExternalRegister = BaseWithVersion + "/externalregister";
            public const string ExternalLogin = BaseWithVersion + "/externallogin";
            public const string Callback = BaseWithVersion + "/externalcallback";
            public const string AlterCookieCallback = BaseWithVersion + "/alterCookieCallback";
            public const string Register = BaseWithVersion + "/register";
            public const string Login = BaseWithVersion + "/login";
            public const string ForgotPassword = BaseWithVersion + "/forgotpassword";
            public const string SendEmailToResetPassword = BaseWithVersion + "/sendemailtoresetpassword";
            public const string ValidateResetPasswordToken = BaseWithVersion + "/validateresetpasswordtoken";
            public const string LogOut = BaseWithVersion + "/logout";
            public const string GetUserEmail = BaseWithVersion + "/getuseremail";
            public const string ChangeProfileInfo = BaseWithVersion + "/changeinfo";
            public const string GetUserInfo = BaseWithVersion + "/getuserinfo";
            public const string IsUserGoogle = BaseWithVersion + "/isUserGoogle";
            public const string ChangeEmail = BaseWithVersion + "/changeEmail";
            public const string ConfirmEmail = BaseWithVersion + "/confirmEmail";
            public const string SendEmailConfirmation = BaseWithVersion + "/sendEmailConfirmation";
        }

        public static class Payment
        {
            public const string Base = "payments";
            public const string BaseWithVersion = Version + "/" + Base;
        }

        public static class SiteGroup
        {
            public const string Base = "sitegroups";
            public const string BaseWithVersion = Version + "/" + Base;
        }

        public static class Site
        {
            public const string Base = "sites";
            public const string List = "list";
            public const string Delete = "delete";
            public const string BaseWithVersion = Version + "/" + Base;
            public const string GetById = BaseWithVersion + "/{id}";
            public const string Update = BaseWithVersion + "/{id}";
            public const string BaseWithVersionList = BaseWithVersion + "/list";
            public const string BaseWithVersionDelete = BaseWithVersion + "/" + Delete + "/{id}";
            public const string GetByUser = BaseWithVersion + "/GetByUser/{id}";
        }

        public static class Domain
        {
            public const string Base = "domains";
            public const string BaseWithVersion = Version + "/" + Base;
            public const string GetById = BaseWithVersion + "/{id}";
        }

        public static class Subscription
        {
            public const string Base = "subscriptions";
            public const string BaseWithVersion = Version + "/" + Base;
            public const string GetGroupedByDuration = Version + "/" + Base + "/byduration";
        }

        public static class SubscriptionPlan
        {
            public const string Base = "subscriptionplan";
            public const string BaseWithVersion = Version + "/" + Base;
        }
        public static class Flag
        {
            public const string Base = "flags";
            public const string BaseWithVersion = Version + "/" + Base;
            public const string GetById = BaseWithVersion + "/{id}";
        }

        public static class Geolocation
        {
            public const string Base = "geolocation";
            public const string BaseWithVersion = Version + "/" + Base;
            public const string GetById = BaseWithVersion + "/{id}";
        }
        public static class Template
        {
            public const string Base = "template";
            public const string BaseWithVersion = Version + "/" + Base;
            public const string GetById = BaseWithVersion + "/{id}";
        }
        public static class Constructor
        {
            public const string Base = "constructor";
            public const string BaseWithVersion = Version + "/" + Base;
            public const string GetById = BaseWithVersion + "/{id}";
        }
        public static class Page
        {
            public const string Base = "page";
            public const string BaseWithVersion = Version + "/" + Base;
            public const string GetById = BaseWithVersion + "/{id}";
            public const string DeleteById = BaseWithVersion + "/{id}";
            public const string CreateWithVersion = Version + "/Create";
        }

        public static class Settings
        {
            public const string Base = "settings";
            public const string BaseWithVersion = Version + "/" + Base;
            public const string GetById = BaseWithVersion + "/{id}";
        }

    }
}
