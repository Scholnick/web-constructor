﻿namespace PortalForArbitrators.Contracts.v1.Requests
{
    public class ConfirmEmailRequest
    {
        public string Code { get; set; }
        public string Email { get; set; }
    }
}
