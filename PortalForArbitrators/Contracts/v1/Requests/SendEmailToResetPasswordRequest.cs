﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalForArbitrators.Contracts.v1.Requests
{
    public class SendEmailToResetPasswordRequest
    {
        public string Email { get; set; }
    }
}
