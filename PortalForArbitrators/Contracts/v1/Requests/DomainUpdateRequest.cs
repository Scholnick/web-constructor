﻿using PortalForArbitrators.Common;
using System;

namespace PortalForArbitrators.Contracts.v1.Responses
{
    public class DomainUpdateRequest
    {        
        public string Name { get; set; }
        public Guid? SiteId { get; set; }
        public string Status { get; set; }
        public DomenType DomenType { get; set; } = DomenType.general;
        public string UserId { get; set; } = null;
    }
}