﻿using PortalForArbitrators.Common;
using System;

namespace PortalForArbitrators.Contracts.v1.Responses
{
    public class DomainCreateRequest
    {
        public string Name { get; set; }
        public string Status { get; set; }
        public DomenType DomenType { get; set; } = DomenType.general;
    }
}