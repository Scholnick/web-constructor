﻿
namespace PortalForArbitrators.Contracts.v1.Requests
{
    public class ChangeEmailRequest
    {
        public string OldEmail { get; set; }
        public string NewEmail { get; set; }
        public string Code { get; set; }
    }
}
