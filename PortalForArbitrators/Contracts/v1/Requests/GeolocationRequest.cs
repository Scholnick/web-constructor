﻿using System;

namespace PortalForArbitrators.Contracts.v1.Responses
{
    public class GeolocationRequest
    {
        public string Title { get; set; }
        public int pupular_index { get; set; }
        public string ImageUrl { get; set; }
    }
}