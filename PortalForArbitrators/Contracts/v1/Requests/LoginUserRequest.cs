﻿using System.ComponentModel.DataAnnotations;

namespace PortalForArbitrators.Contracts.v1.Requests
{
    public class LoginUserRequest
    {
        [Required(ErrorMessage = "Введите, пожалуйста, адрес электронной почты")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Введите, пожалуйста, пароль")]
        public string Password { get; set; }
    }
}