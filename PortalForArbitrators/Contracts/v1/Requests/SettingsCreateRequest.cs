﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace PortalForArbitrators.Contracts.v1.Requests
{
    public class SettingsCreateRequest
    {
        public int PublishNeedHttps { get; set; }
        public int PublishNeedRoute { get; set; }
        public int RedirectToWww { get; set; }
        public int UseTrailingSlash { get; set; }
        public string Metatags { get; set; }
        public string Script { get; set; }

        #region Robots

        public RobotsSettingsCreateRequest Robots { get; set; }
        #endregion

        #region Cookie
        public CookieSettingsCreateRequest Cookie { get; set; }
        #endregion
    }

    public class RobotsSettingsCreateRequest
    {
        public int IsEnable { get; set; } //bool
        public string Body { get; set; }
    }

    public class CookieSettingsCreateRequest
    {
        public int IsEnable { get; set; } //bool
        public string Font { get; set; }
        public string Size { get; set; }
        public string Message { get; set; }
        public string TextButton { get; set; }
        public string Background { get; set; }
        public string ColorMessage { get; set; }
        public string ColorButton { get; set; }
        public string ColorTextButton { get; set; }
    }

}
