﻿using PortalForArbitrators.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace PortalForArbitrators.Contracts.v1.Requests
{
    public class GetCheckpointsDto
    {
        public Site Site { get; set; }
    }
}
