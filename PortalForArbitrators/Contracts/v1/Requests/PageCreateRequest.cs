﻿using System;

namespace PortalForArbitrators.Contracts.v1.Responses
{
    public class PageCreateRequest
    {
        //public int pageIndex { get; set; }
        public Guid Id { get; set; }
        //public string Title { get; set; }
        public string Name { get; set; }

        public int IsPublished { get; set; } //bool
    }
}