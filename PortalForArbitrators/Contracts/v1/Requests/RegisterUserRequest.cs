﻿using System.ComponentModel.DataAnnotations;

namespace PortalForArbitrators.Contracts.v1.Requests
{
    public class RegisterUserRequest
    {
        [EmailAddress(ErrorMessage = "Адрес электронной почты не валиден или пустой")] 
        public string Email { get; set; }
        [StringLength(20, ErrorMessage = "Пароль должен содержать минимум 6 и максимум 20 символов", MinimumLength = 6)]
        public string Password { get; set; }
        [Compare("Password", ErrorMessage = "Пароли не совпадают")]
        public string ConfirmPassword { get; set; }
    }
}   