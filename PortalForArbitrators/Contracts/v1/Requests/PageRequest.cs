﻿using System;

namespace PortalForArbitrators.Contracts.v1.Responses
{
    public class PageRequest
    {
        //public Guid Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string JsonContent { get; set; }
        //public Guid SiteId { get; set; }
        //public Site Site { get; set; }
    }
}