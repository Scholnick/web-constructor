﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PortalForArbitrators.Contracts.v1.Responses;
using PortalForArbitrators.Data.Entities;

namespace PortalForArbitrators.Contracts.v1.Requests
{
    public class CheckpointCreateRequest
    {
        public SiteCreateRuqest Site { get; set; }
    }

    public class SiteCreateRuqest
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public int Type { get; set; }
        public Guid? GeolocationId { get; set; }
        public Guid? TemplateGroupId { get; set; }

        public string Title { get; set; }
        //public string Name { get; set; }

        //public string Status { get; set; }
        public int isTemplate { get; set; } //bool


        //public List<Domain> Domains { get; set; }
        //public List<SubDomain> SubDomains { get; set; }
        //public List<Translation> Translations { get; set; }
        public List<PageCreateRequest> Pages { get; set; }

        public SettingsCreateRequest Settings { get; set; }
    }

}
