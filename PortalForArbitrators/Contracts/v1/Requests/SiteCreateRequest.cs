﻿using System;
using System.Collections.Generic;
using PortalForArbitrators.Common;
using PortalForArbitrators.Data.Entities;

namespace PortalForArbitrators.Contracts.v1.Requests
{
    public class SiteCreateRequest
    {
        public string Title { get; set; }
        public AddressRequest Address { get; set; }

    }
    public class AddressRequest
    {
        public DomenType domenType { get; set; } = DomenType.general;
        public Guid domainId { get; set; }
        public string subdomainName { get; set; }
    }

}