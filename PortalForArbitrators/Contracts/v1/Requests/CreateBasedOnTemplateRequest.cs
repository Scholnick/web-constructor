﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalForArbitrators.Contracts.v1.Requests
{
    public class CreateBasedOnTemplateRequest
    {
        public Guid templateId { get; set; }

        public SiteTemplate Site { get; set; }
    }
    public class SiteTemplate
    {
        public Guid Id { get; set; }

        //"type": 1, //Тип сайта. Значения: 1 - обычный, 2 - маркетинговый
        public int Type { get; set; }
        public Guid? GeolocationId { get; set; }
    }
}