﻿using System;

namespace PortalForArbitrators.Contracts.v1.Responses
{
    public class TemplateFilterRequest
    {
        public int GroupId { get; set; }
    }
}