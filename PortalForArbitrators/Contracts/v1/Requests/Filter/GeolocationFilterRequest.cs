﻿using System;

namespace PortalForArbitrators.Contracts.v1.Responses
{
    public class GeolocationFilterRequest
    {
        public string Title { get; set; }
    }
}