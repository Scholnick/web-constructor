﻿using System;

namespace PortalForArbitrators.Contracts.v1.Requests
{
    public class CreatePaymentRequest
    {
        public decimal Sum { get; set; }
        public string ServiceName { get; set; }
        public string Info { get; set; }
        public string Status { get; set; }
    }
}