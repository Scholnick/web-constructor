﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PortalForArbitrators.Data.Entities;

namespace PortalForArbitrators.Contracts.v1.Requests
{
    public class CheckpointRequest
    {
        public SiteRuqest Site { get; set; }
    }

    public class SiteRuqest
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        public Guid? CheckpointId { get; set; }
        //public CheckpointeRuqest Checkpoint { get; set; }
        public Guid Uuid { get; set; } = Guid.NewGuid();
        public string Title { get; set; }
        public string Name { get; set; }
        public DateTime TimeCreated { get; set; }
        public SiteGroup SiteGroup { get; set; }
        public string Status { get; set; }
        public int isTemplate { get; set; } //bool
        public int isArchive { get; set; } //bool
        public Guid? UserId { get; set; }
        public Guid? SiteGroupId { get; set; }
        public Guid? GeolocationId { get; set; }
        public List<Domain> Domains { get; set; }
        public List<SubDomain> SubDomains { get; set; }
        public List<Translation> Translations { get; set; }
        public List<Page> Pages { get; set; }

        public Guid? SettingsId { get; set; }
        public Settings Settings { get; set; }
    }


}
