﻿using PortalForArbitrators.Data.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalForArbitrators.Contracts.v1.Responses
{
    public class CheckpointResponse
    {
        public Guid Id { get; set; }
        public CheckpointType Type { get; set; }
        public int ToSystemGod { get; set; } //bool
        public DateTime Created { get; set; }
    }
}
