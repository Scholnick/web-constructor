﻿using System.Collections.Generic;

namespace PortalForArbitrators.Contracts.v1.Responses
{
    public class SubscriptionGroupResponse
    {
        public int SubscriptionDurationInDays { get; set; }
        public List<SubscriptionResponse> Subscriptions { get; set; }
    }
}
