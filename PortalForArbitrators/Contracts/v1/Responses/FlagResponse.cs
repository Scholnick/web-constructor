﻿using System;

namespace PortalForArbitrators.Contracts.v1.Responses
{
    public class FlagResponse
    {
        public string Key { get; set; }
        public string Icon { get; set; }
        public string Title { get; set; }
        public int IsPopular { get; set; } //bool
    }
}