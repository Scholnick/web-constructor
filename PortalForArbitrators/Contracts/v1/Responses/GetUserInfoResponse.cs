﻿namespace PortalForArbitrators.Contracts.v1.Responses
{
    public class GetUserInfoResponse
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string ImgPath { get; set; }
        public bool EmailConfirmed { get; set; } //bool
        public bool IsUserGoogle { get; set; } //bool
    }
}
