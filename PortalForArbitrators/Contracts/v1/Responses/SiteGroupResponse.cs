﻿using System.Collections.Generic;
using PortalForArbitrators.Data.Entities;

namespace PortalForArbitrators.Contracts.v1.Responses
{
    public class SiteGroupResponse
    {
        public string SiteGroupName { get; set; }
        public List<SiteInGroupsResponse> Sites { get; set; }
    }
}