﻿using System;

namespace PortalForArbitrators.Contracts.v1.Responses
{
    public class DomainResponse
    {
        public string Url { get; set; }
        public DateTime PaidFrom { get; set; }
        public string Status{ get; set; }
    }
}