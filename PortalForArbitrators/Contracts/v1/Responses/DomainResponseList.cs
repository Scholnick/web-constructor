﻿using PortalForArbitrators.Common;
using System;

namespace PortalForArbitrators.Contracts.v1.Responses
{
    public class DomainResponseList
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public DomenType DomenType { get; set; }
    }
}