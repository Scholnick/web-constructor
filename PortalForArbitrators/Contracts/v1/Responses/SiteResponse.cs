﻿using System.Collections.Generic;
using PortalForArbitrators.Data.Entities;

namespace PortalForArbitrators.Contracts.v1.Responses
{
    public class SiteResponse
    {
        public string Name { get; set; }
        public List<Page> Pages { get; set; }
    }
}
