﻿using PortalForArbitrators.Data.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalForArbitrators.Contracts.v1.Responses
{
    public class CheckpointCreateResponse
    {
        public Guid Id { get; set; }
        public Guid SiteId { get; set; }
        public int Type { get; set; }
        public int ToSystemGod { get; set; } //bool
        public DateTime Created { get; set; }
        //public List<Dictionary<int, string>> pages { get; set; }
        public List<PageShortCreate> pages { get; set; }

    }
    public class PageShortCreate
    {
        public int sort { get; set; }
        public Guid Id { get; set; }
    }


}
