﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalForArbitrators.Contracts.v1.Responses
{
    public class SubscriptionPlanResponse
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
