﻿using System.Collections.Generic;

namespace PortalForArbitrators.Contracts.v1.Responses
{
    public class SiteInGroupsResponse
    {
        public string Name { get; set; }
        public List<string> DomainUrls { get; set; }
    }
}