﻿namespace PortalForArbitrators.Contracts.v1.Responses
{
    public class SubscriptionResponse
    {
        public decimal Price { get; set; }
        public int ProjectsCount { get; set; }
        public int LandingsCount { get; set; }
        public SubscriptionPlanResponse SubscriptionPlan { get; set; }
    }
}