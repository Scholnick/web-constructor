﻿using System;

namespace PortalForArbitrators.Contracts.v1.Responses
{
    public class GeolocationResponse
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public int pupular_index { get; set; }
        public string ImageUrl { get; set; }
    }
}