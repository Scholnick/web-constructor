﻿using System;

namespace PortalForArbitrators.Contracts.v1.Responses
{
    public class RegisterUserResponse
    {
        public string Email { get; set; }
        public DateTime TimeRegistered { get; set; }
    }
}