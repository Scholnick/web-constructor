﻿using System;

namespace PortalForArbitrators.Contracts.v1.Responses
{
    public class PaymentResponse
    {
        public decimal Sum { get; set; }
        public string ServiceName { get; set; }
        public string Info { get; set; }
        public string Status { get; set; }
        public DateTime DateOfOperation { get; set; }
    }
}