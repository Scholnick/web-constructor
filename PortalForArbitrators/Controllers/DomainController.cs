﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PortalForArbitrators.Common;
using PortalForArbitrators.Contracts.v1;
using PortalForArbitrators.Contracts.v1.Responses;
using PortalForArbitrators.Data.Entities;
using PortalForArbitrators.Repositories.Interfaces;
using PortalForArbitrators.Services.Interfaces;

namespace PortalForArbitrators.Controllers
{
    [Route("v1/site")]
    [ApiController]
    public class DomainController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IGenericRepository<Domain> _domainRepository;
        private readonly IIdentityService _identityService;

        public DomainController(IMapper mapper, IGenericRepository<Domain> domainRepository, IIdentityService identityService)
        {
            _mapper = mapper;
            this._domainRepository = domainRepository;
            _identityService = identityService;
        }


        [HttpGet("[controller]/general-list")]
        //[HttpGet(ApiRoutes.Domain.BaseWithVersion)]
        public async Task<IActionResult> GetGeneral()
        {
            //var domains = await _domainRepository.GetAsync(payment => payment.User == _identityService.GetAppUserAsync(this.User).Result);

            var domains = await _domainRepository.GetAsync(x => x.DomenType == DomenType.general);

            var domainsResponse = _mapper.Map<DomainResponseList[]>(domains);

            return Ok(domainsResponse);
        }


        [HttpGet("[controller]/personal-list")]
        //[HttpGet(ApiRoutes.Domain.BaseWithVersion)]
        public async Task<IActionResult> GetPersonal(bool freeDomain = true)
        {
            IEnumerable<Domain> domains = null;
            if (freeDomain)
                domains = await _domainRepository.GetAsync(x => x.DomenType == DomenType.personal && x.Sites.Count == 0);
            else
                domains = await _domainRepository.GetAsync(x => x.DomenType == DomenType.personal);

            var domainsResponse = _mapper.Map<DomainResponseList[]>(domains);

            return Ok(domainsResponse);
        }

        [Authorize]
        //[HttpPost(ApiRoutes.Site.BaseWithVersion)]
        [HttpPost("[controller]/create")]
        public async Task<IActionResult> Create([FromBody] DomainCreateRequest request)
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors.Select(x => x.ErrorMessage));
                return BadRequest(new { errors = errors });
            }
            var domainDb = (await _domainRepository.GetAsync(x => x.Name == request.Name)).FirstOrDefault();
            if (domainDb != null)
            {
                return BadRequest(new { message = "Domain exist" });
            }

            Guid userId = _identityService.GetUserId();
            if (userId == Guid.Empty)
            {
                return BadRequest(new { message = "Wrong userId" });
            }

            //var domain = _mapper.Map<Domain>(request);

            var domain = new Domain();
            domain.Name = request.Name;
            domain.Status = request.Status;
            domain.DomenType = request.DomenType;
            domain.UserId = userId;
            if (await _domainRepository.CreateAsync(domain))
            {
                return Ok(new { id = domain.Id });
            }

            return StatusCode(500);
        }

        [HttpPut("[controller]/edit/{id}")]
        //[HttpPut(ApiRoutes.Site.GetById)]
        public async Task<IActionResult> UpdateSiteContent(Guid id, [FromBody] DomainUpdateRequest request)
        {
            var domain = await _domainRepository.GetByIdAsync(id);

            if (domain == null)
            {
                return NotFound();
            }

            if (await _domainRepository.UpdateAsync(domain))
            {
                return Ok();
            }

            return StatusCode(500);
        }

        [HttpDelete("[controller]/delete/{id}")]
        //[HttpDelete(ApiRoutes.Site.BaseWithVersionDelete)]
        public async Task<IActionResult> Delete(Guid id)
        {
            var site = await _domainRepository.GetByIdAsync(id);

            if (site == null)
            {
                return NotFound();
            }

            if (await _domainRepository.DeleteAsync(site))
            {
                return Ok();
            }

            return StatusCode(500);
        }
    }
}