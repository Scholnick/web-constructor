﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PortalForArbitrators.Contracts.v1;
using PortalForArbitrators.Contracts.v1.Responses;
using PortalForArbitrators.Data.Entities;
using PortalForArbitrators.Repositories.Interfaces;
using PortalForArbitrators.Services.Interfaces;

namespace PortalForArbitrators.Controllers
{
    public class FlagController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IGenericRepository<Flag> _flagRepository;
        private readonly IIdentityService _identityService;

        public FlagController(IMapper mapper, IGenericRepository<Flag> flagRepository, IIdentityService identityService)
        {
            _mapper = mapper;
            this._flagRepository = flagRepository;
            _identityService = identityService;
        }

        [HttpGet(ApiRoutes.Flag.BaseWithVersion)]
        public async Task<IActionResult> Get()
        {
            //var flags = await _flagRepository.GetAsync();

            //var flagResponse = _mapper.Map<FlagResponse[]>(flags);
            var flagResponseList = new List<FlagResponse>() {
                new FlagResponse(){
                    Key= "ua",
                    Icon= "flag-ua",
                    Title= "Україна",
                    IsPopular= 1
                },
                 new FlagResponse(){
                    Key= "uk",
                    Icon= "flag-uk",
                    Title= "Англия",
                    IsPopular= 1
                },
                new FlagResponse(){
                    Key= "es",
                    Icon= "flag-es",
                    Title= "США",
                    IsPopular= 1
                },
                new FlagResponse(){
                    Key= "indo",
                    Icon= "flag-indo",
                    Title= "Индонезия",
                    IsPopular= 1
                },
                new FlagResponse(){
                    Key= "aust",
                    Icon= "flag-aust",
                    Title= "Австралия",
                    IsPopular= 1
                },
                new FlagResponse(){
                    Key= "swedian",
                    Icon= "flag-swedian",
                    Title= "Швеция",
                    IsPopular= 1
                },
                new FlagResponse(){
                    Key= "france",
                    Icon= "flag-france",
                    Title= "Франция",
                    IsPopular= 1
                },
                new FlagResponse(){
                    Key= "germany",
                    Icon= "flag-germany",
                    Title= "Германия",
                    IsPopular= 1
                },
            };

            return Ok(flagResponseList);
        }
    }
}