﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PortalForArbitrators.Contracts.v1;
using PortalForArbitrators.Contracts.v1.Responses;
using PortalForArbitrators.Data.Entities;
using PortalForArbitrators.Repositories.Interfaces;
using PortalForArbitrators.Services.Interfaces;

namespace PortalForArbitrators.Controllers
{
    [Route("v1/site/")]
    [ApiController]
    public class PageController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IGenericRepository<Page> _pageRepository;
        private readonly IIdentityService _identityService;

        public PageController(IMapper mapper, IGenericRepository<Page> pageRepository, IIdentityService identityService)
        {
            _mapper = mapper;
            this._pageRepository = pageRepository;
            _identityService = identityService;
        }

        [HttpGet("[controller]/list")]

        //[HttpGet(ApiRoutes.Page.BaseWithVersion)]
        public async Task<IActionResult> Get()
        {
            try
            {
                var pages = await _pageRepository.GetAsync();

                var pagesResponse = _mapper.Map<PageResponse[]>(pages);

                return Ok(pagesResponse);
            }
            catch (Exception ex)
            {

                return Ok(ex.Message);
            }
        }

        [HttpPost("[controller]/create")]
        //[HttpPost(ApiRoutes.Page.BaseWithVersion)]
        public async Task<IActionResult> Create(PageRequest pageDto)
        {
            var pagesRequest = _mapper.Map<Page>(pageDto);
            pagesRequest.Id = Guid.NewGuid();
            var pages = await _pageRepository.CreateAsync(pagesRequest);

            return Ok();
        }

        [HttpDelete("[controller]/delete/{id}")]
        //[HttpDelete(ApiRoutes.Page.BaseWithVersion)]
        public async Task<IActionResult> Delete(string id)
        {
            var pages = await _pageRepository.GetByIdAsync(id);
            if (pages != null)
            {
                await _pageRepository.DeleteAsync(pages);
            }

            return Ok();
        }

        // PUT api/<PageController>/5
        //[HttpPut(ApiRoutes.Page.BaseWithVersion)]
        [HttpPut("[controller]/edit/{id}")]
        public void UpdateSiteContent(Guid id, [FromBody] List<Page> pages)
        {

        }
    }
}