﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PortalForArbitrators.Common;
using PortalForArbitrators.Contracts.v1;
using PortalForArbitrators.Contracts.v1.Responses;
using PortalForArbitrators.Data.Entities;
using PortalForArbitrators.Repositories.Interfaces;
using PortalForArbitrators.Services.Interfaces;

namespace PortalForArbitrators.Controllers
{
    public class GeolocationController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IGenericRepository<Geolocation> _geolocationRepository;
        private readonly IIdentityService _identityService;

        public GeolocationController(IMapper mapper,
            IGenericRepository<Geolocation> geolocationRepository,
            IIdentityService identityService)
        {
            _mapper = mapper;
            this._geolocationRepository = geolocationRepository;
            _identityService = identityService;
        }

        //[HttpGet(ApiRoutes.Geolocation.BaseWithVersion)]
        [HttpGet("v1/site/[controller]/list")]

        public async Task<IActionResult> Get([FromQuery] GeolocationFilterRequest filter = null, int page = 1, int pageSize = 5)
        {
            try
            {
                IEnumerable<Geolocation> model = null;

                if (filter != null && filter.Title != null)
                {

                    model = await _geolocationRepository.GetAsync(x => x.Title.Contains(filter.Title));

                }
                else
                {
                    model = await _geolocationRepository.GetAsync();
                }

                var result = _mapper.Map<IEnumerable<GeolocationResponse>>(model);

                var resultPage = await result.ToPagedEnumerableAsync(page, pageSize);
                return Ok(resultPage);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpPost(ApiRoutes.Geolocation.BaseWithVersion)]
        public async Task<IActionResult> Create([FromBody] GeolocationRequest model)
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors.Select(x => x.ErrorMessage));
                return BadRequest(new { errors = errors });
            }

            var request = _mapper.Map<Geolocation>(model);
            request.Id = Guid.NewGuid();
            var pages = await _geolocationRepository.CreateAsync(request);

            return Ok();
        }

        [HttpDelete("v1/site/[controller]/delete/{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            var pages = await _geolocationRepository.GetByIdAsync(id);
            if (pages != null)
            {
                await _geolocationRepository.DeleteAsync(pages);
            }

            return Ok();
        }

        // PUT api/<PageController>/5
        [HttpPut("v1/site/[controller]/edit/{id}")]
        public async Task<IActionResult> UpdateAsync(Guid id, [FromBody] Geolocation request)
        {
            var model = await _geolocationRepository.GetByIdAsync(id);
            if (model == null)
            {
                return NotFound();
            }

            model.Id = request.Id;
            model.Title = request.Title;
            model.pupular_index = request.pupular_index;
            model.ImageUrl = request.ImageUrl;

            if (await _geolocationRepository.UpdateAsync(model))
            {
                return Ok();
            }

            return StatusCode(500);
        }
    }
}