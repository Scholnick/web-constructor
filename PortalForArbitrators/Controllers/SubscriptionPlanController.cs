﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PortalForArbitrators.Contracts.v1;
using PortalForArbitrators.Contracts.v1.Responses;
using PortalForArbitrators.Data.Entities;
using PortalForArbitrators.Repositories.Interfaces;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PortalForArbitrators.Controllers
{
    public class SubscriptionPlanController : ControllerBase
    {
        private readonly IGenericRepository<SubscriptionPlan> _subscriptionPlanRepository;
        private readonly IMapper _mapper;

        public SubscriptionPlanController(IGenericRepository<SubscriptionPlan> subscriptionPlanRepository, IMapper mapper)
        {
            _subscriptionPlanRepository = subscriptionPlanRepository;
            _mapper = mapper;
        }

        [HttpGet(ApiRoutes.SubscriptionPlan.BaseWithVersion)]
        public async Task<IActionResult> Get()
        {
            var subscriptionPlans = await _subscriptionPlanRepository.GetAsync(includeFunc: subplans => subplans.Include(subplan => subplan.Subscriptions));

            var subscriptionsResponse = _mapper.Map<List<SubscriptionGroupResponse>>(subscriptionPlans);

            return Ok(subscriptionsResponse);
        }
    }
}
