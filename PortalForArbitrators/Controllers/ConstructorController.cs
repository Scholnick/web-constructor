﻿using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PortalForArbitrators.Contracts.v1;
using PortalForArbitrators.Contracts.v1.Responses;
using PortalForArbitrators.Data.Entities;
using PortalForArbitrators.Repositories.Interfaces;
using PortalForArbitrators.Services.Interfaces;

namespace PortalForArbitrators.Controllers
{
    public class ConstructorController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IGenericRepository<Constructor> _constructorRepository;
        private readonly IIdentityService _identityService;

        public ConstructorController(IMapper mapper,
            IGenericRepository<Constructor> constructorRepository,
            IIdentityService identityService)
        {
            _mapper = mapper;
            this._constructorRepository = constructorRepository;
            _identityService = identityService;
        }

        [HttpGet(ApiRoutes.Constructor.BaseWithVersion)]
        public async Task<IActionResult> Get()
        {
            var domains = await _constructorRepository.GetAsync();

            var domainsResponse = _mapper.Map<Constructor[]>(domains);

            return Ok(domainsResponse);
        }
    }
}