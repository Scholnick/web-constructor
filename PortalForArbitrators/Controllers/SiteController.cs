﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PortalForArbitrators.Common;
using PortalForArbitrators.Contracts.v1.Requests;
using PortalForArbitrators.Contracts.v1.Responses;
using PortalForArbitrators.Data.Entities;
using PortalForArbitrators.Helpers;
using PortalForArbitrators.Repositories.Interfaces;
using PortalForArbitrators.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalForArbitrators.Controllers
{
    [Route("v1/")]
    [ApiController]
    public class SiteController : ControllerBase
    {
        private readonly IGenericRepository<Site> _siteRepository;
        private readonly IGenericRepository<Domain> _domainRepository;
        private readonly IGenericRepository<SubDomain> _subdomainRepository;

        private readonly IMapper _mapper;
        private readonly IIdentityService _identityService;

        public SiteController(
            IGenericRepository<Site> siteRepository,
            IGenericRepository<Domain> domainRepository,
            IGenericRepository<SubDomain> subdomainRepository,
            IMapper mapper,
            IIdentityService identityService)
        {
            _siteRepository = siteRepository;
            _domainRepository = domainRepository;
            _subdomainRepository = subdomainRepository;

            _mapper = mapper;
            _identityService = identityService;
        }

        [Authorize]
        [HttpGet("[controller]/list")]
        //[HttpGet(ApiRoutes.Site.BaseWithVersionList)]
        public async Task<IActionResult> GetSites([FromQuery] SiteFilter filter, int page = 1, int pageSize = 5)
        {
            try
            {
                Guid userId = _identityService.GetUserId();
                if (userId == Guid.Empty)
                {
                    return BadRequest(new { message = "Wrong userId" });
                }

                IEnumerable<Site> site = null;

                if (filter.Status == null && filter.Name == null)
                {
                    site = await _siteRepository.GetAsync(f => f.UserId == userId);
                }
                if (filter.Name != null)
                {
                    site = await _siteRepository.GetAsync(f => f.Name == filter.Name && f.UserId == userId);
                }
                else if (filter.Status != null)
                {
                    site = await _siteRepository.GetAsync(f => f.Status == filter.Status && f.UserId == userId);
                }

                var result = _mapper.Map<Site[]>(site);

                var resultPage = await result.ToPagedEnumerableAsync(page, pageSize);
                return Ok(resultPage);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("[controller]/GetByUser/{id}")]
        //[HttpGet(ApiRoutes.Site.GetByUser)]
        public async Task<IActionResult> GetSiteByUser(Guid UserId)
        {
            var site = await _siteRepository.GetByIdAsync(UserId, sites => sites.Include(site => site.Pages));

            if (site == null)
            {
                return NotFound();
            }

            if (site.User == await _identityService.GetAppUserAsync(this.User))
            {
                return Forbid();
            }

            var siteResponse = _mapper.Map<Site[]>(site);

            return Ok(siteResponse);
        }

        [HttpGet("[controller]/{uuid}")]
        //[HttpGet(ApiRoutes.Site.GetById)]
        public async Task<IActionResult> GetSite(Guid uuid)
        {
            var site = (await _siteRepository.GetAsync(x => x.Uuid == uuid, null,
                sites => sites
                .Include(site => site.Pages)
                .Include(site => site.Checkpoint)
                .Include(site => site.Settings)
                )).OrderByDescending(x => x.TimeCreated).FirstOrDefault();

            if (site == null)
            {
                return NotFound();
            }

            // to do
            //if (site.User == await _identityService.GetAppUserAsync(this.User))
            //{
            //    return Forbid();
            //}

            //var siteResponse = _mapper.Map<Site>(site);

            var siteResponse = site.GetSiteDto();

            return Ok(siteResponse);
        }

        [Authorize]
        //[HttpPost(ApiRoutes.Site.BaseWithVersion)]
        [HttpPost("[controller]/create")]
        public async Task<IActionResult> Create([FromBody] SiteCreateRequest request)
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors.Select(x => x.ErrorMessage));
                return BadRequest(new { errors = errors });
            }

            Guid userId = _identityService.GetUserId();
            if (userId == Guid.Empty)
            {
                return BadRequest(new { message = "Wrong userId" });
            }

            var siteDb = (await _siteRepository.GetAsync(x => x.Title == request.Title)).FirstOrDefault();
            if (siteDb != null)
            {
                return BadRequest(new { message = "Site exist" });
            }

            var domainDb = (await _domainRepository.GetAsync(x => x.Id == request.Address.domainId)).FirstOrDefault();
            if (domainDb == null)
            {
                return BadRequest(new { message = "Domain not found" });
            }

            var site = new Site();
            site.Id = Guid.NewGuid();
            site.Title = request.Title;
            site.Uuid = Guid.NewGuid();
            site.UserId = userId;
            site.TimeCreated = DateTime.Now;
            site.type = SiteType.simple;

            site.DomainId = request.Address.domainId;

            if (await _siteRepository.CreateAsync(site))
            {

                if (request.Address.domenType == DomenType.personal && !string.IsNullOrEmpty(request.Address.subdomainName))
                {
                    var subdomain = new SubDomain();
                    subdomain.Id = Guid.NewGuid();
                    subdomain.DomainId = request.Address.domainId;
                    subdomain.SiteId = site.Id;
                    subdomain.Url = request.Address.subdomainName;

                    await _subdomainRepository.CreateAsync(subdomain);
                }

                return Ok(new { id = site.Id });
            }

            return StatusCode(500);
        }

        [HttpPut("[controller]/edit/{id}")]
        //[HttpPut(ApiRoutes.Site.GetById)]
        public async Task<IActionResult> UpdateSiteContent(Guid id, [FromBody] List<Page> updatedPages)
        {
            var site = await _siteRepository.GetByIdAsync(id, sites => sites.Include(site => site.Pages));

            if (site == null)
            {
                return NotFound();
            }

            for (int i = 0; i < updatedPages.Count; i++)
            {
                for (int j = 0; j < site.Pages.Count; j++)
                {
                    if (updatedPages[i].Id == site.Pages[j].Id)
                    {
                        site.Pages[j] = updatedPages[i];
                    }
                }
            }

            if (await _siteRepository.UpdateAsync(site))
            {
                return Ok(new { message = "Pages were updated" });
            }

            return StatusCode(500);
        }

        [HttpDelete("[controller]/delete/{id}")]
        //[HttpDelete(ApiRoutes.Site.BaseWithVersionDelete)]
        public async Task<IActionResult> Delete(Guid id)
        {
            var site = await _siteRepository.GetByIdAsync(id, sites => sites.Include(site => site.Pages));

            if (site == null)
            {
                return NotFound();
            }

            if (await _siteRepository.DeleteAsync(site))
            {
                return Ok();
            }

            return StatusCode(500);
        }
    }
}
