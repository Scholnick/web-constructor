﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PortalForArbitrators.Contracts.v1;
using PortalForArbitrators.Contracts.v1.Responses;
using PortalForArbitrators.Data.Entities;
using PortalForArbitrators.Repositories.Interfaces;
using PortalForArbitrators.Services.Interfaces;

namespace PortalForArbitrators.Controllers
{
    public class SubscriptionController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IGenericRepository<Subscription> _subscriptionRepository;

        public SubscriptionController(IMapper mapper, 
            IGenericRepository<Subscription> domainRepository, 
            IIdentityService identityService)
        {
            _mapper = mapper;
            this._subscriptionRepository = domainRepository;
        }

        [HttpGet(ApiRoutes.Subscription.BaseWithVersion)]
        public async Task<IActionResult> Get()
        {
            var subscriptions = await _subscriptionRepository.GetAsync();

            var subscriptionsResponse = _mapper.Map<SubscriptionResponse[]>(subscriptions);

            return Ok(subscriptionsResponse);
        }

        [HttpGet(ApiRoutes.Subscription.GetGroupedByDuration)]
        public async Task<IActionResult> GetWithGroup()
        {
            var subscriptions = await _subscriptionRepository.GetWithGroupByAsync(
                subscription => subscription.SubscriptionDurationInDays,
                group => new SubscriptionGroupResponse()
                {
                    SubscriptionDurationInDays = Convert.ToInt32(group.Key),
                    Subscriptions = group.Select(subscription => _mapper.Map<SubscriptionResponse>(subscription)).OrderBy(sub => sub.SubscriptionPlan.Name).ToList()
                },
                subscription => subscription.Include(subscription => subscription.SubscriptionPlan),
                orderBy: subscription => subscription.OrderBy(sub => sub.SubscriptionDurationInDays)
                );

            return Ok(subscriptions);
        }
    }
}