﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PortalForArbitrators.Contracts.v1;
using PortalForArbitrators.Contracts.v1.Requests;
using PortalForArbitrators.Contracts.v1.Responses;
using PortalForArbitrators.Data.Entities;
using PortalForArbitrators.Repositories.Interfaces;
using PortalForArbitrators.Services.Interfaces;

namespace PortalForArbitrators.Controllers
{
    public class SiteGroupController : ControllerBase
    {
        private readonly IGenericRepository<SiteGroup> _siteGroupRepository;
        private readonly IMapper _mapper;
        private readonly IIdentityService _identityService;
        private readonly IGenericRepository<Site> _siteRepository;

        public SiteGroupController(IGenericRepository<SiteGroup> siteGroupRepository, 
            IMapper mapper, 
            IIdentityService identityService, 
            IGenericRepository<Site> siteRepository)
        {
            _siteGroupRepository = siteGroupRepository;
            _mapper = mapper;
            _identityService = identityService;
            _siteRepository = siteRepository;
        }

        [HttpGet(ApiRoutes.SiteGroup.BaseWithVersion)]
        public async Task<IActionResult> Get()
        {
            var responseArticles = await _siteRepository.GetWithGroupByAsync(
                site => site.SiteGroup.Name,
                group => new SiteGroupResponse()
                {
                    SiteGroupName = group.Key.ToString(),
                    Sites = group.Select(site => _mapper.Map<SiteInGroupsResponse>(site)).ToList()
                },
                site => site.Include(site => site.SiteGroup),
                site => site.Status == "Published");

            return Ok(responseArticles);
        }

        //[HttpPost(ApiRoutes.Payment.BaseWithVersion)]
        //public async Task<IActionResult> Create([FromBody] CreatePaymentRequest request)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        var errors = ModelState.Values.SelectMany(v => v.Errors.Select(x => x.ErrorMessage));
        //        return BadRequest(new { errors = errors });
        //    }

        //    var payment = _mapper.Map<Payment>(request);

        //    if (await _siteGroupRepository.CreateAsync(payment))
        //    {
        //        return Ok(new { message = "Payment was created" });
        //    }

        //    return StatusCode(500);
        //}
    }
}