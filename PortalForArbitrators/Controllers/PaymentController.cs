﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PortalForArbitrators.Contracts.v1;
using PortalForArbitrators.Contracts.v1.Requests;
using PortalForArbitrators.Contracts.v1.Responses;
using PortalForArbitrators.Data.Entities;
using PortalForArbitrators.Repositories.Interfaces;
using PortalForArbitrators.Services.Interfaces;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PortalForArbitrators.Controllers
{
    [Route("v1/site")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        private readonly IGenericRepository<Payment> _paymentRepository;
        private readonly IMapper _mapper;
        private readonly IIdentityService _identityService;

        public PaymentController(IGenericRepository<Payment> paymentRepository, IMapper mapper, IIdentityService identityService)
        {
            _paymentRepository = paymentRepository;
            _mapper = mapper;
            _identityService = identityService;
        }

        [HttpGet("[controller]/list")]
        //[HttpGet(ApiRoutes.Payment.BaseWithVersion)]
        public async Task<IActionResult> Get()
        {
            var payments = await _paymentRepository.GetAsync(/*payment => payment.User == _identityService.GetAppUserAsync(this.User).Result*/);

            var paymentsResponse = _mapper.Map<PaymentResponse[]>(payments);

            return Ok(paymentsResponse);
        }

        [HttpPost("[controller]/create")]
        //[HttpPost(ApiRoutes.Payment.BaseWithVersion)]
        public async Task<IActionResult> Create([FromBody] CreatePaymentRequest request)
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors.Select(x => x.ErrorMessage));
                return BadRequest(new { errors = errors });
            }
            
            var payment = _mapper.Map<Payment>(request);

            if (await _paymentRepository.CreateAsync(payment))
            {
                return Ok(new { message = "Payment was created" });
            }

            return StatusCode(500);
        }

        // PUT v1/<PaymentController>/5
        //[HttpPut("{id}")]
        [HttpPut("[controller]/edit/{id}")]
        public void UpdateSiteContent(Guid id, [FromBody] List<Page> pages)
        {

        }

        // DELETE v1/<PaymentController>/5
        //[HttpDelete("{id}")]
        [HttpDelete("[controller]/delete/{id}")]
        public void Delete(int id)
        {
        }
    }
}
