﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PortalForArbitrators.Contracts.v1;
using PortalForArbitrators.Contracts.v1.Requests;
using PortalForArbitrators.Contracts.v1.Responses;
using PortalForArbitrators.Data.Entities;
using PortalForArbitrators.Services.Interfaces;
using PortalForArbitrators.Dtos;
using PortalForArbitrators.Helpers;
using System.IO;
using System.Net.Http.Headers;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore.Query;

namespace PortalForArbitrators.Controllers
{
    [ApiController]
    public class IdentityController : ControllerBase
    {
        private readonly IIdentityService _identityService;
        private readonly IMapper _mapper;
        private readonly IEmailService _emailService;

        public IdentityController(IIdentityService identityService, IMapper mapper, IEmailService emailService)
        {
            _identityService = identityService;
            _mapper = mapper;
            _emailService = emailService;
        }

        [HttpPost("​v1​​/identity​/UserList")]
        public async Task<IActionResult> UsersList()
        {
            var result = await _identityService.GetAllUsersAsync();
            //var user = _mapper.Map<Userdto>(result);

            return Ok(result);
        }

        [HttpPost("​v1​​/identity​/createTestUser")]
        public async Task<IActionResult> CreateTestUser(RegisterUserRequest registerRequest)
        {
            if (registerRequest == null)
            {
                return BadRequest(new { message = "Registration information was null" });
            }

            if (!ModelState.IsValid)
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors.Select(x => x.ErrorMessage));
                return BadRequest(new { errors = errors });
            }

            var user = _mapper.Map<User>(registerRequest);
            string requestReferer = Request.Headers["Referer"].ToString();

            var result = await _identityService.RegisterAsync(user, registerRequest.Password);

            if (result.Success)
            {
                var userRes = await _identityService.FindUserByEmailAsync(registerRequest.Email);
                userRes.EmailConfirmed = true;
                var token = await _identityService.GenerateToken(registerRequest.Email);
                return Ok(new { userid = userRes.Id, token = token });
            }

            return BadRequest(new { errors = result.Errors.Select(error => error.Description) });
        }


        [HttpPost(ApiRoutes.Identity.ExternalLogin)]
        [HttpPost(ApiRoutes.Identity.ExternalRegister)]
        public IActionResult ExternalAuthenticate(string returnUrl)
        {
            var callbackUrl = $"{Request.Scheme}://{Request.Host.Value}/{ApiRoutes.Identity.Callback}" +
                $"?returnUrl={returnUrl}";
            var challengeResultData = _identityService.ExternalAuthenticate(callbackUrl);
            return new ChallengeResult(challengeResultData.ProviderName, challengeResultData.AuthenticationProperties);
        }

        [HttpGet(ApiRoutes.Identity.Callback)]
        public async Task<IActionResult> ExternalAuthenticateCallbackAsync(string returnUrl, string remoteError = null)
        {
            if (remoteError != null)
            {
                return BadRequest(new { message = $"Error from external provider: {remoteError}" });
            }

            var alterCookieUrl = $"{Request.Scheme}://{Request.Host.Value}" +
                                 $"/{ApiRoutes.Identity.AlterCookieCallback}" +
                                 $"?returnUrl={returnUrl}";

            if (await _identityService.TryExternalLoginAsync())
            {
                return Redirect(alterCookieUrl);
            }

            if (await _identityService.RegisterExternalAsync())
            {
                return Redirect(alterCookieUrl);
            }

            if (Request.Cookies.Any(cookie => cookie.Key == "Identity.External"))
            {
                Response.Cookies.Delete("Identity.External");
            }

            string errorUrl = returnUrl + (returnUrl.Contains("?") ? "&" : "?") + "googleautherror=true";

            return Redirect(errorUrl);
        }

        [HttpPost(ApiRoutes.Identity.Register)]
        public async Task<IActionResult> Register(RegisterUserRequest registerRequest)
        {
            if (registerRequest == null)
            {
                return BadRequest(new { message = "Registration information was null" });
            }

            if (!ModelState.IsValid)
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors.Select(x => x.ErrorMessage));
                return BadRequest(new { errors = errors });
            }

            var user = _mapper.Map<User>(registerRequest);
            string requestReferer = Request.Headers["Referer"].ToString();

            var result = await _identityService.RegisterAsync(user, registerRequest.Password);

            if (result.Success)
            {
                //var code = _identityService.EncodeToken(await _identityService.GenerateConfirmationEmailToken(user));
                //var linkParameters = requestReferer.Split("/");
                //var link = $"{linkParameters[0]}://{linkParameters[2]}?confirmemail=true&emailtoconfirm={user.Email}&code={code}";

                //try
                //{
                //    await _emailService.SendEmailAsync(user.Email, "Confirm email", $"Link to confirm your email - {HtmlEncoder.Default.Encode(link)}");
                //}
                //catch
                //{
                //    return BadRequest(new { error = "На сервере произошла ошибка" });
                //}
                //var response = _mapper.Map<RegisterUserResponse>(user);
                //return Created("", response);
                return Ok();
            }

            return BadRequest(new { errors = result.Errors.Select(error => error.Description) });
        }

        [HttpPost(ApiRoutes.Identity.Login)]
        public async Task<IActionResult> Login(LoginUserRequest loginRequest)
        {
            if (loginRequest == null)
            {
                return BadRequest(new { message = "Login information was null" });
            }

            if (!ModelState.IsValid)
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors.Select(x => x.ErrorMessage));
                return BadRequest(new { errors = errors });
            }

            var result = await _identityService.LoginAsync(loginRequest);

            if (result != null)
            {
                return Ok(new { token = result });
            }

            return BadRequest(new { errors = new List<string>() { "К сожалению, нет пользователя с такой почтой и/или паролем" } });
        }

        [HttpPost(ApiRoutes.Identity.SendEmailToResetPassword)]
        public async Task<IActionResult> SendEmailToResetPassword([FromBody] SendEmailToResetPasswordRequest req)
        {
            var user = await GetUser();

            if (user == null)
            {
                return BadRequest(new { errors = new List<string>() { "К сожалению, нет пользователя с такой почтой" } });
            }

            var code = _identityService.EncodeToken(await _identityService.GetResetPasswordTokenAsync(user));

            string requestReferer = Request.Headers["Referer"].ToString();

            var link = $"{requestReferer.Split("/")[0]}://{requestReferer.Split("/")[2]}?forgotpassword=true&code={code}&email={user.Email}";

            await _emailService.SendEmailAsync(user.Email, "Forgot password", $"Link to recover your password - {HtmlEncoder.Default.Encode(link)}");

            return Ok();
        }

        [HttpPost(ApiRoutes.Identity.ForgotPassword)]
        public async Task<IActionResult> ForgotPassword([FromBody] ResetPasswordRequest resetPasswordRequest)
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors.Select(x => x.ErrorMessage));
                return BadRequest(new { errors = errors });
            }

            var user = await GetUser();

            if ((DateTime.Now - user.LastTimeEmailForResetPasswordSent).Minutes < 2)
            {
                return BadRequest(new { errors = "Password recovery email has already been sent" });
            }

            if (user == null)
            {
                return BadRequest(new { errors = new List<string>() { "К сожалению, нет пользователя с такой почтой" } });
            }

            var result = await _identityService.ResetPasswordAsync(user, _identityService.DecodeToken(resetPasswordRequest.Code), resetPasswordRequest.Password);

            if (result.Succeeded)
            {
                user.LastTimeEmailForResetPasswordSent = DateTime.Now;
                return Ok();
            }

            return BadRequest(new { errors = result.Errors.Select(error => error.Description) });
        }

        [HttpGet(ApiRoutes.Identity.ValidateResetPasswordToken)]
        public async Task<IActionResult> ValidateResetPasswordToken(string code, string email)
        {
            if (string.IsNullOrEmpty(code) || string.IsNullOrEmpty(email))
            {
                return BadRequest(new { errors = new List<string>() { "Вы передали пустые параметры" } });
            }

            var user = await GetUser();

            if (user == null)
            {
                return BadRequest(new { errors = new List<string>() { "К сожалению, нет пользователя с такой почтой" } });
            }

            var isTokenValid = await _identityService.VerifyResetPasswordTokenAsync(user, _identityService.DecodeToken(code));
            string message = isTokenValid ? "Token is valid." : "Token is not valid";
            return Ok(new { message = message });
        }

        [HttpPost(ApiRoutes.Identity.LogOut)]
        public async Task<IActionResult> LogOut()
        {
            Response.Cookies.Delete("Token");
            //await _identityService.LogOut();
            return Ok(new { message = "User logged out" });
        }

        [HttpGet(ApiRoutes.Identity.GetUserEmail)]
        public async Task<IActionResult> GetUserEmail()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return BadRequest(new { errors = new List<string>() { "Пользователь не аутентифицирован" } });
            }

            return Ok(new { userEmail = (await GetUser())?.Email });
        }

        [HttpPut(ApiRoutes.Identity.ChangeProfileInfo), DisableRequestSizeLimit]
        public async Task<IActionResult> ChangeUserInfo([FromForm] ChangeUserProfileRequest changeUserProfileRequest)
        {
            var errorsList = new List<string>();
            var successMessagesList = new List<string>();

            if (!User.Identity.IsAuthenticated)
            {
                return Unauthorized();
            }

            if (!CheckObjectForNullHelper.CheckIfAnyPropertyIsNotNull(changeUserProfileRequest))
            {
                return Ok();
            }

            var user = await GetUser(users => users.Include(user => user.Avatar));
            var changeUserInfoModel = _mapper.Map<ChangeUserProfileRequest, ChangeUserInfoModel>(changeUserProfileRequest);
            if (changeUserInfoModel.FirstName == user.FirstName && changeUserInfoModel.LastName == user.LastName)
            {
                changeUserInfoModel.FirstName = null;
                changeUserInfoModel.LastName = null;
            }

            var avatar = changeUserProfileRequest.Avatar;

            if (avatar != null)
            {
                var result = await _identityService.SetAvatarForChangeUserModel(changeUserInfoModel, avatar);

                if (!result)
                {
                    errorsList.Add("Картинка была неверного формата или возникла ошибка на сервере");
                }
            }

            if (CheckObjectForNullHelper.CheckIfAnyPropertyIsNotNull(changeUserInfoModel))
            {
                var updatedUser = _mapper.Map(changeUserInfoModel, user);
                var result = await _identityService.ChangeUserInfo(updatedUser);

                if (!result)
                {
                    errorsList.Add("Не удалось сменить данные пользователя. Случилась ошибка на сервере");
                }
                else
                {
                    successMessagesList.Add("Данные пользователя были успешно изменены");
                }
            }

            string email = changeUserProfileRequest.Email;

            if (!await _identityService.IsGoogleUser(user) && email != user.Email)
            {
                if (!user.EmailConfirmed)
                {
                    errorsList.Add("Подтвердите почту для смены почты");
                }
                else
                {
                    string requestReferer = Request.Headers["Referer"].ToString();

                    var emailAttr = new EmailAddressAttribute();
                    var isEmailValid = emailAttr.IsValid(email);

                    if (isEmailValid)
                    {
                        var result = await _identityService.CheckIfUserWithEmailExists(email);

                        if (result)
                        {
                            errorsList.Add("Пользователь с такой почтой уже существует");
                        }
                        else
                        {
                            var code = _identityService.EncodeToken(await _identityService.GetResetEmailTokenAsync(user, changeUserProfileRequest.Email));
                            var linkParameters = requestReferer.Split("/");
                            var link = $"{linkParameters[0]}://{linkParameters[2]}?forgotemail=true&code={code}&newemail={changeUserProfileRequest.Email}&oldemail={user.Email}";

                            try
                            {
                                await _emailService.SendEmailAsync(changeUserProfileRequest.Email, "Change email", $"Link to change your email - {HtmlEncoder.Default.Encode(link)}");
                                successMessagesList.Add($"Письмо с подтверждением смены почты было отослано на {changeUserProfileRequest.Email}");
                            }
                            catch (Exception ex)
                            {
                                errorsList.Add("Произошла ошибка при отсылке письма о смене почты");
                            }
                        }
                    }
                    else
                    {
                        errorsList.Add("Email был введен неверно");
                    }
                }
            }

            var resetpasswordmodel = _mapper.Map<ChangePasswordModel>(changeUserProfileRequest);

            if (CheckObjectForNullHelper.CheckIfAnyPropertyIsNotNull(resetpasswordmodel) && !await _identityService.IsGoogleUser(user))
            {
                var context = new System.ComponentModel.DataAnnotations.ValidationContext(resetpasswordmodel, serviceProvider: null, items: null);
                var results = new List<ValidationResult>();

                var isResetModelValid = Validator.TryValidateObject(resetpasswordmodel, context, results, true);

                if (isResetModelValid)
                {
                    var result = await _identityService.ChangePasswordByOldPassword(user, resetpasswordmodel);

                    if (!result.Succeeded)
                    {
                        result.Errors.Select(error => error.Description).ToList().ForEach(error => errorsList.Add(error));
                    }
                    else
                    {
                        successMessagesList.Add("Password was successfully changed");
                    }
                }
                else
                {
                    results.Select(error => error.ErrorMessage).ToList().ForEach(error => errorsList.Add(error));
                }

            }

            if (errorsList.Count == 0)
            {
                return Ok(new { successMessages = successMessagesList });
            }

            return BadRequest(new { errors = errorsList, successMessages = successMessagesList });
        }

        [HttpGet(ApiRoutes.Identity.GetUserInfo)]
        public async Task<IActionResult> GetUserInfo()
        {
            var user = await GetUser(users => users.Include(user => user.Avatar));
            if (user != null)
            {
                var userInfoResponse = _mapper.Map<GetUserInfoResponse>(user);
                userInfoResponse.IsUserGoogle = await _identityService.IsGoogleUser(user);
                return Ok(userInfoResponse);
            }

            return Unauthorized();
        }

        [HttpPut(ApiRoutes.Identity.ChangeEmail)]
        public async Task<IActionResult> ChangeEmail([FromBody] ChangeEmailRequest changeEmailRequest)
        {
            var user = await _identityService.FindUserByEmailAsync(changeEmailRequest.OldEmail);

            var result = await _identityService.ChangeEmailAsync(user, _identityService.DecodeToken(changeEmailRequest.Code), changeEmailRequest.NewEmail);

            if (!result.Succeeded)
            {
                return StatusCode(500, new { errors = result.Errors.Select(error => error.Description) });
            }

            var newUser = await _identityService.FindUserByEmailAsync(changeEmailRequest.NewEmail);

            newUser.EmailConfirmed = false;

            var resultChangeUserInfo = await _identityService.ChangeUserInfo(newUser);

            if (!resultChangeUserInfo)
            {
                return StatusCode(500);
            }

            string requestReferer = Request.Headers["Referer"].ToString();

            var code = _identityService.EncodeToken(await _identityService.GenerateConfirmationEmailToken(user));
            var linkParameters = requestReferer.Split("/");
            var link = $"{linkParameters[0]}://{linkParameters[2]}?confirmemail=true&emailtoconfirm={user.Email}&code={code}";

            try
            {
                await _emailService.SendEmailAsync(user.Email, "Confirm email", $"Link to confirm your email - {HtmlEncoder.Default.Encode(link)}");
            }
            catch
            {
                return BadRequest(new { error = "На сервере произошла ошибка" });
            }

            return Ok(new { message = "Почта была удачно изменена" });
        }

        [HttpPost(ApiRoutes.Identity.SendEmailConfirmation)]
        public async Task<IActionResult> SendEmailConfirmation()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Unauthorized();
            }

            string requestReferer = Request.Headers["Referer"].ToString();

            var user = await GetUser();

            var code = _identityService.EncodeToken(await _identityService.GenerateConfirmationEmailToken(user));
            var linkParameters = requestReferer.Split("/");
            var link = $"{linkParameters[0]}://{linkParameters[2]}?confirmemail=true&emailtoconfirm={user.Email}&code={code}";

            try
            {
                await _emailService.SendEmailAsync(user.Email, "Confirm email", $"Link to confirm your email - {HtmlEncoder.Default.Encode(link)}");
                return Ok(new { message = "Письмо с подтверждением почты было отправлено" });
            }
            catch
            {
                return BadRequest(new { error = "На сервере произошла ошибка" });
            }
        }

        [HttpPost(ApiRoutes.Identity.ConfirmEmail)]
        public async Task<IActionResult> ConfirmEmail([FromBody] ConfirmEmailRequest confirmEmailRequest)
        {
            var user = await _identityService.FindUserByEmailAsync(confirmEmailRequest.Email);

            var result = await _identityService.ConfirmEmail(user, _identityService.DecodeToken(confirmEmailRequest.Code));

            if (!result.Succeeded)
            {
                return BadRequest(new { errors = result.Errors.Select(error => error.Description) });
            }

            return Ok(new { message = "The email was confirmed" });
        }

        private async Task<User> GetUser(Func<IQueryable<User>, IIncludableQueryable<User, object>> includeFunc = null)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            return await _identityService.FindUserByIdAsync(userId, includeFunc);
        }

        /// <summary>
        ///     This endpoint gets requested right after the login or registration & login
        ///     process was performed. The user agent is redirected here, and ASP.NET Core
        ///     Identity cookie gets updated to use SameSite=None.
        /// </summary>
        /// <param name="returnUrl"> 
        ///     The URL to redirect the user agent after successful cookie update. 
        /// </param>
        /// <returns></returns>
        [HttpGet(ApiRoutes.Identity.AlterCookieCallback)]
        public IActionResult AlterCookieCallback(string returnUrl)
        {
            const string AUTH_COOKIE_NAME = ".AspNetCore.Identity.Application";

            if (!HttpContext.Request.Cookies.ContainsKey(AUTH_COOKIE_NAME))
            {
                return BadRequest(new { message = "Registration failed." });
            }
            string cookieValue = HttpContext.Request.Cookies[AUTH_COOKIE_NAME];

            HttpContext.Response.Cookies.Delete(AUTH_COOKIE_NAME);
            HttpContext.Response.Cookies.Append(AUTH_COOKIE_NAME, cookieValue,
                options: new Microsoft.AspNetCore.Http.CookieOptions()
                {
                    Secure = true,
                    HttpOnly = true,
                    SameSite = Microsoft.AspNetCore.Http.SameSiteMode.None
                });

            returnUrl ??= _identityService.GetRedirectUrl();
            return Redirect(returnUrl);
        }

        [HttpGet("/v1/identity/cookie")]
        public IActionResult Cookie()
        {
            const string AUTH_COOKIE_NAME = ".AspNetCore.Identity.Application";

            if (!HttpContext.Request.Cookies.ContainsKey(AUTH_COOKIE_NAME))
            {
                return BadRequest(new { message = "Registration failed." });
            }
            string cookieValue = HttpContext.Request.Cookies[AUTH_COOKIE_NAME];

            return Ok(cookieValue);
        }
    }
}