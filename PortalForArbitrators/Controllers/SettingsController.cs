﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PortalForArbitrators.Contracts.v1;
using PortalForArbitrators.Contracts.v1.Responses;
using PortalForArbitrators.Data.Entities;
using PortalForArbitrators.Repositories.Interfaces;
using PortalForArbitrators.Services.Interfaces;

namespace PortalForArbitrators.Controllers
{
    //[Route("v1/[controller]")]
    [ApiController]
    public class SettingsController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IGenericRepository<Settings> _settingRepository;
        private readonly IIdentityService _identityService;

        public SettingsController(IMapper mapper, IGenericRepository<Settings> settingRepository, IIdentityService identityService)
        {
            _mapper = mapper;
            this._settingRepository = settingRepository;
            _identityService = identityService;
        }

        [HttpGet(ApiRoutes.Settings.BaseWithVersion)]
        public async Task<IActionResult> Get()
        {
            var model = await _settingRepository.GetAsync();

            //var modelResponse = _mapper.Map<Settings[]>(model);

            return Ok(model);
        }

        [HttpGet(ApiRoutes.Settings.GetById)]
        public async Task<IActionResult> Get(Guid id)
        {
            var model = await _settingRepository.GetByIdAsync(id, sites => sites
            .Include(site => site.metatags)
            .Include(site => site.Robots)
            .Include(site => site.Cookie));

            var modelResponse = _mapper.Map<Settings[]>(model);

            return Ok(modelResponse);
        }

        [HttpPost("v1/[controller]/create")]
        public async Task<IActionResult> Create([FromBody] Settings request)
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors.Select(x => x.ErrorMessage));
                return BadRequest(new { errors = errors });
            }
            //Guid userId;
            //var userIdParse = Guid.TryParse(request.UserId, out userId);
            //if (!userIdParse)
            //{
            //    return BadRequest(new { message = "Wrong userId" });
            //}
            var templateNew = new Settings();

            templateNew.publishNeedHttps = request.publishNeedHttps;
            templateNew.publishNeedRoute = request.publishNeedRoute;
            templateNew.useTrailingSlash = request.useTrailingSlash;
            templateNew.metatags = request.metatags;
            templateNew.Robots.isEnable = request.Robots.isEnable;
            templateNew.Robots.body = request.Robots.body;
            templateNew.script = request.script;

            templateNew.Cookie.isEnable = request.Cookie.isEnable;
            templateNew.Cookie.font = request.Cookie.font;
            templateNew.Cookie.size = request.Cookie.size;
            templateNew.Cookie.message = request.Cookie.message;
            templateNew.Cookie.textButton = request.Cookie.textButton;
            templateNew.Cookie.background = request.Cookie.background;
            templateNew.Cookie.colorMessage = request.Cookie.colorMessage;
            templateNew.Cookie.colorButton = request.Cookie.colorButton;
            templateNew.Cookie.colorTextButton = request.Cookie.colorTextButton;

            var template = _mapper.Map<Settings>(request);

            if (await _settingRepository.CreateAsync(templateNew))
            {
                return Ok();
            }

            return StatusCode(500);
        }

        [HttpPut("v1/[controller]/edit/{id}")]
        public async Task<IActionResult> UpdateSiteContent(Guid id, [FromBody] Template request)
        {
            var template = await _settingRepository.GetByIdAsync(id);

            if (template == null)
            {
                return NotFound();
            }

            //domain.Name = request.Name;
            //domain.SiteId = request.SiteId;
            //domain.Status = request.Status;
            //domain.DomenType = request.DomenType;
            //domain.UserId = request.UserId;

            if (await _settingRepository.UpdateAsync(template))
            {
                return Ok();
            }

            return StatusCode(500);
        }

        [HttpDelete("v1/[controller]/delete/{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var site = await _settingRepository.GetByIdAsync(id);

            if (site == null)
            {
                return NotFound();
            }

            if (await _settingRepository.DeleteAsync(site))
            {
                return Ok();
            }

            return StatusCode(500);
        }
    }
}