﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PortalForArbitrators.Common;
using PortalForArbitrators.Contracts.v1.Requests;
using PortalForArbitrators.Contracts.v1.Responses;
using PortalForArbitrators.Data.Entities;
using PortalForArbitrators.Helpers;
using PortalForArbitrators.Repositories.Interfaces;
using PortalForArbitrators.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace PortalForArbitrators.Controllers
{
    [Route("v1/site/[controller]")]
    [ApiController]
    public class CheckpointController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IGenericRepository<Checkpoint> _checkpointRepository;
        private readonly IGenericRepository<Site> _siteRepository;
        private readonly IGenericRepository<Template> _templateRepository;

        private readonly IIdentityService _identityService;

        public CheckpointController(IMapper mapper,
            IGenericRepository<Checkpoint> settingRepository,
            IGenericRepository<Site> siteRepository,
            IGenericRepository<Template> templateRepository,
            IIdentityService identityService)
        {
            _mapper = mapper;
            this._checkpointRepository = settingRepository;
            _templateRepository = templateRepository;
            _siteRepository = siteRepository;
            _identityService = identityService;
        }


        [HttpGet("list/{Uuid}")]
        public async Task<IActionResult> GetCheckpoints(Guid Uuid)
        {
            try
            {
                var sites = await _siteRepository.GetAsync(x => x.Uuid == Uuid);
                var checkpoints = sites.Where(x => x.CheckpointId != null).Select(x => x.CheckpointId).ToList();

                IEnumerable<Checkpoint> model = null;

                var checkpointResponseDto = new List<CheckpointResponse>();
                var checkpointResponse = new List<Checkpoint>();

                foreach (var item in checkpoints)
                {
                    var mod = await _checkpointRepository.GetByIdAsync(item);
                    checkpointResponse.Add(mod);
                }
                model = await _checkpointRepository.GetAsync();

                var result = _mapper.Map<IEnumerable<CheckpointResponse>>(model);

                //var resultPage = await result.ToPagedEnumerableAsync(page, pageSize);
                return Ok(result);

                //return null;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Authorize]
        [HttpPost("create")]
        public async Task<IActionResult> Create([FromBody] CheckpointCreateRequest request)
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors.Select(x => x.ErrorMessage));
                return BadRequest(new { errors = errors });
            }

            Guid userId = _identityService.GetUserId();
            if (userId == Guid.Empty)
            {
                return BadRequest(new { message = "Wrong userId" });
            }

            try
            {
                var siteSet = _mapper.Map<Settings>(request.Site.Settings);
                var siteDb = await _siteRepository.GetByIdAsync(request.Site.Id);

                if (siteDb == null)
                {
                    return BadRequest("Site does not exist");
                }

                var site = new Site();
                site.Id = Guid.NewGuid();
                site.Uuid = siteDb.Uuid;
                site.Title = siteDb.Title;
                site.Name = siteDb.Name;
                site.isTemplate = siteDb.isTemplate;
                site.isArchive = siteDb.isArchive;
                site.UserId = siteDb.UserId;
                site.GeolocationId = request.Site.GeolocationId;
                site.SiteGroupId = siteDb.SiteGroupId;

                site.Settings = siteSet;
                site.TimeCreated = DateTime.Now;

                site.Checkpoint = new Checkpoint()
                {
                    Id = Guid.NewGuid(),
                    Created = DateTime.Now,
                    ToSystemGod = 0,
                    Type = CheckpointType.system,
                    UserId = userId
                };


                if (await _siteRepository.CreateAsync(site))
                {
                    var model = new CheckpointCreateResponse();

                    return Ok(site.Checkpoint);
                }
            }
            catch (Exception ex)
            {
                var errror = ex.Message;

            }

            return StatusCode(500);
        }

        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var checkpoint = await _checkpointRepository.GetByIdAsync(id);
            if (checkpoint == null)
            {
                return NotFound();
            }

            try
            {
                var site = (await _siteRepository.GetAsync(x => x.CheckpointId == checkpoint.Id)).FirstOrDefault();
                await _siteRepository.DeleteAsync(site);

                if (await _checkpointRepository.DeleteAsync(checkpoint))
                {
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { message = ex.Message });
            }

            return StatusCode(500);
        }

        [HttpGet("restore/{id}")]
        public async Task<IActionResult> Restore(Guid id)
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors.Select(x => x.ErrorMessage));
                return BadRequest(new { errors = errors });
            }

            var site = (await _siteRepository.GetAsync(x => x.CheckpointId == id, null, inc => inc.Include(s => s.Settings))).FirstOrDefault();

            if (site == null)
            {
                return NotFound();
            }

            var siteResponse = site.GetSiteDto();

            return Ok(siteResponse);

            //return StatusCode(500);
        }

        [Authorize]
        [HttpPost("createBasedOnTemplate")]
        public async Task<IActionResult> CreateBasedOnTemplate(CreateBasedOnTemplateRequest model)
        {
            var template = await _templateRepository.GetByIdAsync(model.templateId);
            if (template == null)
            {
                return BadRequest(new { message = "Template not foud" });
            }

            var site = await _siteRepository.GetByIdAsync(model.Site.Id);
            if (site == null)
            {
                return BadRequest(new { message = "Site not foud" });
            }

            Guid userId = _identityService.GetUserId();
            if (userId == Guid.Empty)
            {
                return BadRequest(new { message = "Wrong userId" });
            }

            Site SiteNew = new Site();

            SiteNew.Checkpoint = new Checkpoint()
            {
                Id = Guid.NewGuid(),
                Created = DateTime.Now,
                ToSystemGod = 0,
                Type = CheckpointType.system,
                UserId = userId
            };
            SiteNew.Uuid = site.Uuid;
            SiteNew.Title = site.Title;
            SiteNew.Name = site.Name;
            SiteNew.TimeCreated = site.TimeCreated;
            SiteNew.Status = site.Status;
            SiteNew.isTemplate = site.isTemplate;
            SiteNew.Status = site.Status;
            SiteNew.isTemplate = site.isTemplate;
            SiteNew.isArchive = site.isArchive;
            SiteNew.SiteGroupId = site.SiteGroupId;
            SiteNew.SiteGroup = site.SiteGroup;
            SiteNew.UserId = userId;
            SiteNew.User = site.User;
            SiteNew.type = SiteType.marketing;

            if (model.Site.GeolocationId == null)
            {
                SiteNew.GeolocationId = model.Site.GeolocationId;
                SiteNew.type = SiteType.simple;
            }

            SiteNew.type = SiteType.simple;

            SiteNew.Domain = site.Domain;
            //SiteNew.SubDomains = site.SubDomains;
            SiteNew.Translations = site.Translations;
            SiteNew.Pages = site.Pages;
            SiteNew.SettingsId = site.SettingsId;
            SiteNew.Settings = site.Settings;

            await _siteRepository.CreateAsync(SiteNew);
            var siteResponse = site.GetSiteDto();

            return Ok(siteResponse);


            //return StatusCode(500);
        }


    }
}
