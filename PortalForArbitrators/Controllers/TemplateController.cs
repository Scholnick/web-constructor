﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PortalForArbitrators.Common;
using PortalForArbitrators.Contracts.v1;
using PortalForArbitrators.Contracts.v1.Responses;
using PortalForArbitrators.Data.Entities;
using PortalForArbitrators.Repositories.Interfaces;
using PortalForArbitrators.Services.Interfaces;

namespace PortalForArbitrators.Controllers
{
    public class TemplateController : ControllerBase
    {
        private readonly IGenericRepository<Template> _templateRepository;
        private readonly IMapper _mapper;
        private readonly IIdentityService _identityService;

        public TemplateController(IMapper mapper, 
            IGenericRepository<Template> templateRepository, 
            IIdentityService identityService)
        {
            _mapper = mapper;
            this._templateRepository = templateRepository;
            _identityService = identityService;
        }

        //[HttpGet(ApiRoutes.Template.BaseWithersion)]

        [HttpGet("v1/site/[controller]/list")]
        public async Task<IActionResult> Get([FromQuery] TemplateFilterRequest filter, int page = 1, int pageSize = 5)
        {
            try
            {
                IEnumerable<Template> model = null;

                if (filter.GroupId != 0)
                {
                    model = await _templateRepository.GetAsync();
                }
                else
                {
                    model = await _templateRepository.GetAsync();

                }

                var result = _mapper.Map<Template[]>(model);

                var resultPage = await result.ToPagedEnumerableAsync(page, pageSize);
                return Ok(resultPage);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("[controller]/create")]
        public async Task<IActionResult> Create([FromBody] TemplateCreateRequest request)
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors.Select(x => x.ErrorMessage));
                return BadRequest(new { errors = errors });
            }

            var templateDb = (await _templateRepository.GetAsync(x => x.Title == request.Title)).FirstOrDefault();
            if (templateDb != null)
            {
                return BadRequest(new { message = "Template exist" });
            }

            //Guid userId;
            //var userIdParse = Guid.TryParse(request.UserId, out userId);
            //if (!userIdParse)
            //{
            //    return BadRequest(new { message = "Wrong userId" });
            //}

            var template = _mapper.Map<Template>(request);
            template.Id = Guid.NewGuid();
            if (await _templateRepository.CreateAsync(template))
            {
                return Ok();
            }

            return StatusCode(500);
        }

        [HttpPut("[controller]/edit/{id}")]
        public async Task<IActionResult> Update(Guid id, [FromBody] Template request)
        {
            var template = await _templateRepository.GetByIdAsync(id);

            if (template == null)
            {
                return NotFound();
            }

            template.Id = request.Id;
            template.Key = request.Key;
            template.Icon = request.Icon;
            template.Title = request.Title;
            template.IsPopular = request.IsPopular;

            if (await _templateRepository.UpdateAsync(template))
            {
                return Ok();
            }

            return StatusCode(500);
        }

        [HttpDelete("[controller]/delete/{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var site = await _templateRepository.GetByIdAsync(id);

            if (site == null)
            {
                return NotFound();
            }

            if (await _templateRepository.DeleteAsync(site))
            {
                return Ok();
            }

            return StatusCode(500);
        }
    }
}