﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalForArbitrators.Helpers
{
    public static class GuidHelper
    {
        public static Guid? ToGuid(this string guidText)
        {
            Guid userId;
            var userIdParse = Guid.TryParse(guidText, out userId);
            if (!userIdParse)
            {
                return null;
            }

            return userId;
        }
    }
}
