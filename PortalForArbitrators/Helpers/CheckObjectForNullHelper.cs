﻿using System;
using System.Linq;

namespace PortalForArbitrators.Helpers
{
    public static class CheckObjectForNullHelper
    {
        public static bool CheckIfAnyPropertyIsNotNull(object Object)
        {
            return Object.GetType()
                .GetProperties() //get all properties on object
                .Select(pi => pi.GetValue(Object)) //get value for the propery
                .Any(value => !(IsNullOrEmpty(value)));
        }

        public static bool CheckIfThereIsNotNullOrWhiteSpaceString(params string[] values)
        {
            foreach (var value in values)
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    return true;
                }
            }

            return false;
        }

        public static bool CheckIfAllStringsAreNotNullOrWhitespace(params string[] values)
        {
            bool result = true;

            foreach (var value in values)
            {
                result = result && !string.IsNullOrWhiteSpace(value);
            }

            return result;
        }

        private static bool IsNullOrEmpty(object value)
        {
            if (Object.ReferenceEquals(value, null))
                return true;

            var type = value.GetType();

            if (value is string)
            {
                return string.IsNullOrEmpty((string)value);
            }

            return false;
        }
    }
}
