﻿using PortalForArbitrators.Contracts.v1.Responses;
using PortalForArbitrators.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

using System.Threading.Tasks;


namespace PortalForArbitrators.Helpers
{
    public static class MapperHelper
    {
        public static SiteOneResponse GetSiteDto(this Site site)
        {
            SiteOneResponse siteResponse = new SiteOneResponse();

            siteResponse.Id = site.Id;
            siteResponse.CheckpointId = site.CheckpointId;
            siteResponse.Checkpoint = site.Checkpoint;
            siteResponse.Uuid = site.Uuid;
            siteResponse.Title = site.Title;
            siteResponse.Name = site.Name;
            siteResponse.TimeCreated = site.TimeCreated;
            siteResponse.Status = site.Status;
            siteResponse.isTemplate = site.isTemplate;
            siteResponse.Status = site.Status;
            siteResponse.isTemplate = site.isTemplate;
            siteResponse.isArchive = site.isArchive;
            siteResponse.SiteGroupId = site.SiteGroupId;
            siteResponse.SiteGroup = site.SiteGroup;
            siteResponse.UserId = site.UserId;
            siteResponse.User = site.User;
            siteResponse.GeolocationId = site.GeolocationId;
            siteResponse.Geolocation = site.Geolocation;

            siteResponse.Domain = site.Domain;
            //siteResponse.SubDomains = site.SubDomains;
            siteResponse.Translations = site.Translations;
            siteResponse.Pages = site.Pages;
            siteResponse.SettingsId = site.SettingsId;
            siteResponse.Settings = site.Settings;

            return siteResponse;
        }
    }
}
