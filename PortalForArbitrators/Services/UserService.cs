﻿using AutoMapper;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using PortalForArbitrators.Common;
using PortalForArbitrators.Contracts.v1.Requests;
using PortalForArbitrators.Data;
using PortalForArbitrators.Data.Entities;
using PortalForArbitrators.Helpers;
using PortalForArbitrators.Repositories.Interfaces;
using PortalForArbitrators.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace PortalForArbitrators.Services
{
    public class UserService : IUserService
    {
        private readonly IMapper _mapper;
        private readonly IGenericRepository<User> _repository;
        private readonly AppSettings _appSettings;


        public UserService(IMapper mapper, IGenericRepository<User> repository, IOptions<AppSettings> appSettings)
        {
            _repository = repository;
            _mapper = mapper;
            _appSettings = appSettings.Value;

        }

        public async Task<IdentityResultApp> CreateAsync(User user, string password)
        {
            var result = new IdentityResultApp() { Success = false, Errors = new List<IdentityErrorApp>() };
            if (string.IsNullOrWhiteSpace(password))
            {
                //throw new Exception("Password is required");
                result.Errors.Concat(new List<IdentityErrorApp>() { new IdentityErrorApp() { Description = "Password is required" } });
            }

            if ((await _repository.GetAsync(u => u.Email == user.Email)).Any())
            {

                //throw new Exception($@"Email {user.Email} is already taken");
                result.Errors.Concat(new List<IdentityErrorApp>() { new IdentityErrorApp() { Description = $@"Email {user.Email} is already taken" } });
            }


            byte[] passwordHash, passwordSalt;
            CreatePasswordHash(password, out passwordHash, out passwordSalt);

            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;

            var status = await _repository.CreateAsync(user);

            return result;
        }

        public async Task<IdentityResultApp> AuthenticateAsync(string email, string password)
        {
            if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password))
                return null;

            var user = (await _repository.GetAsync(u => u.Email == email)).FirstOrDefault();

            // check if username exists
            if (user == null)
                return null;

            // check if password is correct
            if (!VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
                return null;

            // authentication successful
            //var status = await _repository.CreateAsync(user);

            var result = new IdentityResultApp() { Success = true };
            return result;
        }


        public async Task<string> GenerateToken(string email)
        {
            var user = (await _repository.GetAsync(x => x.Email == email)).FirstOrDefault();

            if (user == null)
            {
                return null;
                //return BadRequest(new { message = "К сожалению, логин или пароль неверны" });
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                    new Claim(ClaimTypes.Name, user.UserName??""),
                    new Claim(ClaimTypes.Email, user.Email),
                    new Claim(ClaimTypes.Role, user.Role==null?"":user.Role.Name)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            return tokenString;
        }

        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            if (storedHash.Length != 64) throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            if (storedSalt.Length != 128) throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordHash");

            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                return computedHash.SequenceEqual(storedHash);
            }
        }
    }
}
