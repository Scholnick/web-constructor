﻿using PortalForArbitrators.Common;
using PortalForArbitrators.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalForArbitrators.Services.Interfaces
{
    public interface IUserService
    {
        Task<IdentityResultApp> CreateAsync(User user, string password);

        Task<string> GenerateToken(string email);
        Task<IdentityResultApp> AuthenticateAsync(string email, string password);

    }
}
