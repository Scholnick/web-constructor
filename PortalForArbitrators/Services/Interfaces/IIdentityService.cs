﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using PortalForArbitrators.Contracts.v1.Requests;
using PortalForArbitrators.Data.Entities;
using PortalForArbitrators.Dtos;
using System.Linq;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.AspNetCore.Http;
using FastTrack_Platform_POC.Models;
using System.Collections.Generic;
using PortalForArbitrators.Common;

namespace PortalForArbitrators.Services.Interfaces
{
    public interface IIdentityService
    {
        ChallengeResultData ExternalAuthenticate(string callbackUrl);
        Task<bool> TryExternalLoginAsync();
        Task<bool> RegisterExternalAsync();
        string GetRedirectUrl();
        Task<IdentityResultApp> RegisterAsync(User user, string password);
        Task<string> LoginAsync(LoginUserRequest loginUserRequest);
        Task<User> GetAppUserAsync(ClaimsPrincipal claimsPrincipal);
        Task<string> GetResetPasswordTokenAsync(User user);
        Task<User> FindUserByEmailAsync(string email);
        Task<IdentityResult> ResetPasswordAsync(User user, string code, string password);
        Task<bool> VerifyResetPasswordTokenAsync(User user, string code);
        string EncodeToken(string code);
        string DecodeToken(string code);
        Task LogOut();
        Task<bool> ChangeUserInfo(User user);
        Task<IdentityResult> ChangePasswordByOldPassword(User user, ChangePasswordModel resetPasswordModel);
        Task<string> GetResetEmailTokenAsync(User user, string newEmail);
        Task<IdentityResult> ChangeEmailAsync(User user, string code, string newEmail);
        Task<bool> SetAvatarForChangeUserModel(ChangeUserInfoModel changeUserInfoModel, IFormFile avatar);
        Task<bool> IsGoogleUser(User user);
        Task<bool> CheckIfUserWithEmailExists(string email);
        Task<string> GenerateConfirmationEmailToken(User user);
        Task<IdentityResult> ConfirmEmail(User user, string token);
        Task<User> FindUserByIdAsync(string userId, Func<IQueryable<User>, IIncludableQueryable<User, object>> includeFunc = null);
        //Task<IdentityResult> CreateRoleAsync(string roleName);
        //Task<IdentityResult> AddToRoleAsync(Guid userId, string roleName);
        //Task<bool> HasRole(string email, string role);
        Task<List<User>> GetAllUsersAsync();

        Task<string> GenerateToken(string email);
        Guid GetUserId();
    }
}
