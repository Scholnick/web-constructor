﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Security.Claims;
using System.Threading.Tasks;
using PortalForArbitrators.Contracts.v1.Requests;
using PortalForArbitrators.Data.Entities;
using PortalForArbitrators.Options;
using PortalForArbitrators.Services.Interfaces;
using System;
using System.Text;
using Microsoft.AspNetCore.WebUtilities;
using PortalForArbitrators.Dtos;
using AutoMapper;
using System.Linq;
using Microsoft.EntityFrameworkCore.Query;
using System.IO;
using Microsoft.AspNetCore.Http;
using FastTrack_Platform_POC.Models;
using System.Collections.Generic;
using PortalForArbitrators.Common;
using PortalForArbitrators.Repositories.Interfaces;

namespace PortalForArbitrators.Services
{
    public class IdentityService : IIdentityService
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly AuthOptions _authOptions;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IMapper _mapper;

        private ExternalLoginInfo _externalLoginInfo;
        private readonly IUserService _userService;
        private readonly IGenericRepository<User> _repository;

        public readonly Guid UserId;


        public IdentityService(
            IUserService userService,
             IGenericRepository<User> repository,
            //UserManager<User> userManager,
            //SignInManager<User> signInManager,
            IConfiguration configuration,
            //RoleManager<IdentityRole> roleManager,
            IMapper mapper,
            IHttpContextAccessor httpContextAccessor)
        {
            //_userManager = userManager;
            //_signInManager = signInManager;
            //_roleManager = roleManager;
            _mapper = mapper;
            _userService = userService;
            _repository = repository;


            _authOptions = configuration.GetSection(nameof(AuthOptions)).Get<AuthOptions>();
            Guid.TryParse(httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier), out var userId);
            UserId = userId;
        }

        private async Task<ExternalLoginInfo> GetExternalInfoAsync() =>
            _externalLoginInfo ??= await _signInManager.GetExternalLoginInfoAsync();

        public ChallengeResultData ExternalAuthenticate(string redirectUrl)
        {
            var properties = _signInManager.ConfigureExternalAuthenticationProperties(
                _authOptions.ProviderName, redirectUrl);
            return new ChallengeResultData { ProviderName = _authOptions.ProviderName, AuthenticationProperties = properties };
        }

        public async Task<bool> TryExternalLoginAsync()
        {
            var information = await GetExternalInfoAsync();
            if (information == null)
            {
                return false;
            }

            var signinResult = await _signInManager
                .ExternalLoginSignInAsync(information.LoginProvider, information.ProviderKey, isPersistent: false, bypassTwoFactor: true);
            return signinResult.Succeeded;
        }

        public async Task<bool> RegisterExternalAsync()
        {
            var information = await GetExternalInfoAsync();
            string[] names = information.Principal.Identity.Name?.Split(' ');
            string email = information.Principal.FindFirstValue(ClaimTypes.Email);

            var user = new User
            {
                UserName = email,
                Email = email,
                FirstName = names?[0] ?? "Unknown Name",
                LastName = names?[1] ?? "Unknown LastName"
            };

            var result = await _userManager.CreateAsync(user);
            if (result.Succeeded)
            {
                result = await _userManager.AddLoginAsync(user, information);
                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: true, information.LoginProvider);
                    return true;
                }

                await _userManager.DeleteAsync(user);
            }

            return false;
        }

        public string GetRedirectUrl() => _authOptions.ReturnURL;


        public async Task<string> GenerateToken(string email)
        {
            var token = await _userService.GenerateToken(email);
            return token;
        }
        public async Task<IdentityResultApp> RegisterAsync(User user, string password)
        {
            var result = await _userService.CreateAsync(user, password);

            return result;
        }

        public async Task<string> LoginAsync(LoginUserRequest loginUserRequest)
        {
            var res = await _userService.AuthenticateAsync(loginUserRequest.Email, loginUserRequest.Password);
            if (res != null && res.Success)
            {
                var token = await _userService.GenerateToken(loginUserRequest.Email);
                return token;
            }

            return null;
        }

        public async Task<User> GetAppUserAsync(ClaimsPrincipal claimsPrincipal) => await _userManager.GetUserAsync(claimsPrincipal);

        public async Task<User> FindUserByIdAsync(string userId, Func<IQueryable<User>, IIncludableQueryable<User, object>> includeFunc = null)
        {
            var user = await _repository.GetByIdAsync(userId, includeFunc);

            return user;
        }

        public async Task<string> GetResetPasswordTokenAsync(User user) => await _userManager.GeneratePasswordResetTokenAsync(user);

        public async Task<User> FindUserByEmailAsync(string email)
        {
            var users = await _repository.GetAsync(x => x.Email == email); ;

            var user = users.FirstOrDefault();

            return user;
        }

        public async Task<IdentityResult> ResetPasswordAsync(User user, string code, string password) => await _userManager.ResetPasswordAsync(user, code, password);

        public async Task<bool> VerifyResetPasswordTokenAsync(User user, string code) => await _userManager.VerifyUserTokenAsync(user, this._userManager.Options.Tokens.PasswordResetTokenProvider, "ResetPassword", code);

        public string EncodeToken(string code)
        {
            var encbuff = Encoding.UTF8.GetBytes(code);
            return WebEncoders.Base64UrlEncode(encbuff);
        }

        public string DecodeToken(string code)
        {
            var codedBytes = WebEncoders.Base64UrlDecode(code);
            return Encoding.UTF8.GetString(codedBytes);
        }

        public async Task LogOut()
        {
            //await _signInManager.SignOutAsync();
        }

        public async Task<bool> ChangeUserInfo(User user)
        {
            if (user != null)
            {
                var result = await _userManager.UpdateAsync(user);

                return result.Succeeded;
            }

            return false;
        }

        public async Task<IdentityResult> ChangePasswordByOldPassword(User user, ChangePasswordModel resetPasswordModel)
        {
            return await _userManager.ChangePasswordAsync(user, resetPasswordModel.OldPassword, resetPasswordModel.NewPassword);
        }

        public async Task<string> GetResetEmailTokenAsync(User user, string newEmail) => await _userManager.GenerateChangeEmailTokenAsync(user, newEmail);

        public async Task<IdentityResult> ChangeEmailAsync(User user, string code, string newEmail)
        {
            var result = await _userManager.ChangeEmailAsync(user, newEmail, code);

            if (result.Succeeded)
            {
                result = await _userManager.SetUserNameAsync(user, newEmail);
            }

            return result;
        }

        public async Task<bool> SetAvatarForChangeUserModel(ChangeUserInfoModel changeUserInfoModel, IFormFile avatar)
        {
            if (!avatar.IsImage())
            {
                return false;
            }

            if (avatar.Length > 0)
            {
                var folderName = Path.Combine("Resources", "Images");
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                var fileName = Guid.NewGuid().ToString() + Path.GetExtension(avatar.FileName);
                var fullPath = Path.Combine(pathToSave, fileName);
                var dbPath = Path.Combine(folderName, fileName);

                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    await avatar.CopyToAsync(stream);
                }

                changeUserInfoModel.Avatar = new Image() { FileName = fileName, FilePath = dbPath };

                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<bool> IsGoogleUser(User user)
        {
            var info = await GetExternalInfoAsync();

            if (info != null)
            {
                return info.LoginProvider == "Google";
            }

            var userlogin = (await _userManager.GetLoginsAsync(user)).FirstOrDefault();

            if (userlogin != null)
            {
                return userlogin.LoginProvider == "Google";
            }

            return false;
        }

        public async Task<bool> CheckIfUserWithEmailExists(string email)
        {
            return await _userManager.FindByEmailAsync(email) != null;
        }

        public async Task<string> GenerateConfirmationEmailToken(User user)
        {
            return await _userManager.GenerateEmailConfirmationTokenAsync(user);
        }

        public async Task<IdentityResult> ConfirmEmail(User user, string token)
        {
            return await _userManager.ConfirmEmailAsync(user, token);
        }

        //public async Task<IdentityResult> CreateRoleAsync(string roleName)
        //{
        //    var role = new IdentityRole();
        //    role.Name = roleName;
        //    var result = await _roleManager.CreateAsync(role);
        //    return result;
        //}

        //public async Task<IdentityResult> AddToRoleAsync(Guid userId, string roleName)
        //{
        //    var user = await GetAppUserAsync(userId);

        //    if (user == null)
        //    {
        //        return IdentityResult.Failed(new IdentityError[] { 
        //            new IdentityError { Description = "User was not found in database" } 
        //        });
        //    }

        //    if(!(await _roleManager.RoleExistsAsync(roleName)))
        //    {
        //        var roleCreateResult = await CreateRoleAsync(roleName);

        //        if (!roleCreateResult.Succeeded)
        //        {
        //            return roleCreateResult;
        //        }
        //    }

        //    var result = await _userManager.AddToRoleAsync(user, roleName);
        //    return result;
        //}

        //public async Task<bool> HasRole(string email, string role) => await _userManager.IsInRoleAsync(await _userManager.FindByNameAsync(email), role);

        public async Task<List<User>> GetAllUsersAsync() => await _userManager.Users.ToListAsync();

        public Guid GetUserId()
        {
            return UserId;
        }
    }
}
