﻿using Microsoft.Extensions.Configuration;
using PortalForArbitrators.Options;
using PortalForArbitrators.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace PortalForArbitrators
{
    public class EmailService : IEmailService
    {
        private EmailSendOptions emailSendOptions;
        public EmailService(IConfiguration configuration)
        {
            this.emailSendOptions = new EmailSendOptions();

            configuration.GetSection(nameof(EmailSendOptions)).Bind(emailSendOptions);
        }
        public async Task SendEmailAsync(string emailTo, string subject, string message)
        {
            // отправитель - устанавливаем адрес и отображаемое в письме имя
            MailAddress from = new MailAddress(emailSendOptions.Email, "Portal For Arbitrators");
            // кому отправляем
            MailAddress to = new MailAddress(emailTo);
            // создаем объект сообщения
            MailMessage m = new MailMessage(from, to);
            // тема письма
            m.Subject = subject;
            // текст письма
            m.Body = message;
            // письмо представляет код html
            m.IsBodyHtml = true;
            // адрес smtp-сервера и порт, с которого будем отправлять письмо
            SmtpClient smtp = new SmtpClient(emailSendOptions.SmtpHost, emailSendOptions.Port);
            smtp.EnableSsl = true;
            // логин и пароль
            smtp.Credentials = new NetworkCredential(emailSendOptions.Email, emailSendOptions.Password);
            await smtp.SendMailAsync(m);
        }
    }
}
