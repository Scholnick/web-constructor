﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalForArbitrators.Common
{
    /// <summary>
    /// Info about page
    /// </summary>
    public class PageInfo
    {
        /// <summary>
        /// номер текущей страницы
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// кол-во объектов на странице
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// всего объектов
        /// </summary>
        public int TotalItems { get; set; }

        /// <summary>
        /// всего страниц
        /// </summary>
        public int TotalPages
        {
            get { return (int)Math.Ceiling((decimal)TotalItems / PageSize); }
        }
    }

    /// <summary>
    /// Result info per page
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PageViewModel<T>
    {
        /// <summary>
        /// List items
        /// </summary>
        public IEnumerable<T> itemsPerPage { get; set; }

        /// <summary>
        /// Info about page
        /// </summary>
        public PageInfo PageInfo { get; set; }

        public PageViewModel()
        {
        }

        public PageViewModel(IEnumerable<T> itemsPerPage, int totalItems, int pageNumber, int pageSize)
        {
            this.itemsPerPage = itemsPerPage;
            PageInfo = new PageInfo
            {
                TotalItems = totalItems,
                PageNumber = pageNumber,
                PageSize = pageSize
            };
        }
    }
}
