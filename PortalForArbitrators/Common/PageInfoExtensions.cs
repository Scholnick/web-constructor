﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalForArbitrators.Common
{
    public static class PageInfoExtensions
    {
        public async static Task<PageViewModel<T>> ToPagedEnumerableAsync<T>(this IEnumerable<T> enumerable, int? pageIndex, int? pageSize)
        {
            return await Task.Run(() => {
                var result = new PageViewModel<T>();
                if (pageIndex != null && pageSize != null)
                {
                    var items = enumerable
                        .Skip((pageIndex.Value - 1) * pageSize.Value)
                        .Take(pageSize.Value).ToList();

                    result.itemsPerPage = items;
                    result.PageInfo = new PageInfo
                    {
                        PageNumber = pageIndex.Value,
                        PageSize = pageSize.Value,
                        TotalItems = enumerable.Count()
                    };
                }
                else
                {
                    var items = enumerable.ToList();

                    result.itemsPerPage = items;
                    result.PageInfo = new PageInfo
                    {
                        PageNumber = pageIndex.Value,
                        PageSize = pageSize.Value,
                        TotalItems = items.Count
                    };
                }
                return result;
            });
        }

        public static async Task<PageViewModel<T>> ToPagedAsync<T>(this IQueryable<T> queryable, int? pageIndex, int? pageSize)
        {
            var result = new PageViewModel<T>();
            if (pageIndex != null && pageSize != null)
            {
                result.itemsPerPage = await queryable
                    .Skip((pageIndex.Value - 1) * pageSize.Value)
                    .Take(pageSize.Value)
                    .ToListAsync();

                result.PageInfo = new PageInfo
                {
                    PageNumber = pageIndex.Value,
                    PageSize = pageSize.Value,
                    TotalItems = await queryable.CountAsync()
                };
            }
            else
            {
                var items = await queryable
                    .ToListAsync();

                result.itemsPerPage = items;

                result.PageInfo = new PageInfo
                {
                    PageNumber = pageIndex.Value,
                    PageSize = pageSize.Value,
                    TotalItems = items.Count
                };
            }
            return result;
        }

        public static PageViewModel<TDst> Map<TSrc, TDst>(this PageViewModel<TSrc> model, IMapper mapper)
        {
            return new PageViewModel<TDst>(model.itemsPerPage.Select(mapper.Map<TSrc, TDst>), model.PageInfo.TotalItems, model.PageInfo.PageNumber, model.PageInfo.PageSize);
        }
    }
}

