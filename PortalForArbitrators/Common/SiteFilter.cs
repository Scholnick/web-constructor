﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalForArbitrators.Common
{
    public class SiteFilter
    {
        public string Name { get; set; }
        public string Status { get; set; }
    }
}
