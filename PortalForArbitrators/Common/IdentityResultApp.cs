﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalForArbitrators.Common
{
    public class IdentityResultApp
    {
        public bool Success { get; set; }
        //public static bool success { get => this.Success; protected set; }
        public IEnumerable<IdentityErrorApp> Errors { get; set; }
    }

    public class IdentityErrorApp
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
