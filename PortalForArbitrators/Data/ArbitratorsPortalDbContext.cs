﻿
using Microsoft.EntityFrameworkCore;
using PortalForArbitrators.Data.Entities;

namespace PortalForArbitrators.Data
{
    public class ArbitratorsPortalDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<Domain> Domains { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<Page> Pages { get; set; }
        public DbSet<Parameter> Parameters { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<Site> Sites { get; set; }
        public DbSet<SiteGroup> SiteGroups { get; set; }
        public DbSet<SubDomain> SubDomains { get; set; }
        public DbSet<Subscription> Subscriptions { get; set; }
        public DbSet<Translation> Translations { get; set; }
        public DbSet<Template> Templates { get; set; }
        public DbSet<Constructor> Constructor { get; set; }


        public DbSet<Settings> Settings { get; set; }
        public DbSet<RobotsSettings> RobotsSettings { get; set; }
        public DbSet<CookieSettings> CookieSettings { get; set; }
        public DbSet<Grids> Grids { get; set; }
        public DbSet<Checkpoint> Checkpoints { get; set; }

        public ArbitratorsPortalDbContext(DbContextOptions<ArbitratorsPortalDbContext> options) : base(options) { }
    }
}