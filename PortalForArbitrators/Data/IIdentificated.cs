﻿using System;

namespace PortalForArbitrators.Data
{
    public interface IIdentificated
    {
        public Guid Id { get; set; }
    }
}