﻿using System;

namespace PortalForArbitrators.Data.Entities
{
    public class Page : IIdentificated
    {
        /// <summary>
        /// PK
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Уникальный идентификатор страницы
        /// </summary>
        public Guid Uuid { get; set; }

        /// <summary>
        /// Название страницы
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Подпись страницы
        /// </summary>
        public string Title { get; set; }

        public string Url { get; set; }
        //public string JsonContent { get; set; }

        /// <summary>
        /// id сайта
        /// </summary>
        public Guid? SiteId { get; set; }
        public Site Site { get; set; }

        /// <summary>
        /// Флаг "опубликован"
        /// </summary>
        public int isPublished { get; set; } //bool

        /// <summary>
        /// Дата/время создания
        /// </summary>
        public DateTime Created { get; set; }
    }
}