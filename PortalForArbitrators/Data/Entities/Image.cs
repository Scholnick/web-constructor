﻿using System;

namespace PortalForArbitrators.Data.Entities
{
    public class Image : IIdentificated
    {
        public Guid Id { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public User User { get; set; }
    }
}