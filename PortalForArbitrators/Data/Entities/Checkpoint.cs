﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalForArbitrators.Data.Entities
{
    public class Checkpoint : IIdentificated
    {
        public Guid Id { get; set; }
        public CheckpointType Type { get; set; }
        public int ToSystemGod { get; set; } //bool
        public DateTime Created { get; set; }
        public Guid UserId { get; set; }
        public User User { get; set; }
    }
}
