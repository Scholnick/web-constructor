﻿
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace PortalForArbitrators.Data.Entities
{
    public class Subscription : IIdentificated
    {
        public Guid Id { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Price { get; set; }
        public int ProjectsCount { get; set; }
        public int LandingsCount { get; set; }
        public int SubscriptionDurationInDays { get; set; }
        public SubscriptionPlan SubscriptionPlan { get; set; }
        public Guid SubscriptionPlanId { get; set; }
    }
}
