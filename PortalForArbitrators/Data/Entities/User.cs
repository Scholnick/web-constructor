﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace PortalForArbitrators.Data.Entities
{
    public class User : IIdentificated
    {
        [Key]
        public Guid Id { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime TimeRegistered { get; set; }
        public bool IsWithSubscription { get; set; }
        public DateTime SubscriptionUntil { get; set; }
        public Subscription Subscription { get; set; }
        public Guid? SubscriptionId { get; set; }
        public List<Site> Sites { get; set; }
        public List<SiteGroup> SiteGroups { get; set; }
        public DateTime LastTimeEmailForResetPasswordSent { get; set; }
        public List<Domain> Domains { get; set; }
        public List<Payment> Payments { get; set; }
        public Image Avatar { get; set; }
        public Guid? AvatarId { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Balance { get; set; }



        public string UserName { get; set; }

        [Required]
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }

        [Display(Name = "Хеш пароля")]
        public byte[] PasswordHash { get; set; }

        [Display(Name = "Соль хеша пароля")]
        public byte[] PasswordSalt { get; set; }

        //[Required]
        public Guid? RoleId { get; set; }

        public Role Role { get; set; }

    }
}