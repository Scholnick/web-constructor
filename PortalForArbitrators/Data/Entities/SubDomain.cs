﻿using System;

namespace PortalForArbitrators.Data.Entities
{
    public class SubDomain : IIdentificated
    {
        public Guid Id { get; set; }
        public Domain Domain { get; set; }
        public Guid DomainId { get; set; }
        public Site Site { get; set; }
        public Guid? SiteId { get; set; }
        public string Url { get; set; }
    }
}