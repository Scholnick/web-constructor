﻿using System;
using System.Collections.Generic;

namespace PortalForArbitrators.Data.Entities
{
    public class Constructor : IIdentificated
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<Elements> Elements { get; set; }
    }
}