﻿using System;

namespace PortalForArbitrators.Data.Entities
{
    public class Translation : IIdentificated
    {
        public Guid Id { get; set; }
        public string Status { get; set; }
        public DateTime Created { get; set; }
        public string SymbolsAmount { get; set; }
        public Site Site { get; set; }
        public Guid? SiteId { get; set; }
    }
}
