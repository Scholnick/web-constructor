﻿using System;
using System.Collections.Generic;

namespace PortalForArbitrators.Data.Entities
{
    public class Elements : IIdentificated
    {
        public Guid Id { get; set; }
        public string type { get; set; }
        public string background { get; set; }
        public string backgroundType { get; set; }
        public string classCss { get; set; }
        public string height { get; set; }
        public string text { get; set; }
        public string image { get; set; }
        public List<Controls> controls { get; set; }
    }
}