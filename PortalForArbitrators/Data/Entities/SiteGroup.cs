﻿using System;
using System.Collections.Generic;

namespace PortalForArbitrators.Data.Entities
{
    public class SiteGroup : IIdentificated
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public User User { get; set; }
        public string UserId { get; set; }
        public List<Site> Sites { get; set; }

    }
}