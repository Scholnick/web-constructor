﻿using System;
using System.Collections.Generic;

namespace PortalForArbitrators.Data.Entities
{
    public class Controls : IIdentificated
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}