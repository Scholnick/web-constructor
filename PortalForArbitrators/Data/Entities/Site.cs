﻿using PortalForArbitrators.Common;
using PortalForArbitrators.Contracts.v1.Responses;
using System;
using System.Collections.Generic;

namespace PortalForArbitrators.Data.Entities
{
    public class Site : IIdentificated
    {
        public Guid Id { get; set; }

        public Guid? CheckpointId { get; set; }
        public Checkpoint Checkpoint { get; set; }
        public Guid Uuid { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public DateTime TimeCreated { get; set; }
        public SiteType type { get; set; }
        public string Status { get; set; }
        public int isTemplate { get; set; } //bool
        public int isArchive { get; set; } //bool
        public Guid? SiteGroupId { get; set; }
        public SiteGroup SiteGroup { get; set; }
        public Guid UserId { get; set; }
        public User User { get; set; }
        public Guid? GeolocationId { get; set; }
        public Geolocation Geolocation { get; set; }

        public Guid? DomainId { get; set; }
        public Domain Domain { get; set; }

        //public Guid? SubDomainId { get; set; }
        //public SubDomain SubDomain { get; set; }

        public List<Translation> Translations { get; set; }
        public List<Page> Pages { get; set; }

        public Guid? SettingsId { get; set; }
        public Settings Settings { get; set; }

    }
}