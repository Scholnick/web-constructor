﻿using PortalForArbitrators.Common;
using System;
using System.Collections.Generic;

namespace PortalForArbitrators.Data.Entities
{
    public class Domain : IIdentificated
    {
        public Guid Id { get; set; }
        public string Name { get; set; }//Url
        public DateTime PaidBefore { get; set; }
        public DateTime InWorkFrom { get; set; }
        //public Site Site { get; set; }
        //public Guid? SiteId { get; set; }
        public string Status { get; set; }
        public DomenType DomenType { get; set; } = DomenType.general;
        public User User { get; set; }
        public Guid UserId { get; set; }
        public List<SubDomain> SubDomains { get; set; }
        public List<Site> Sites { get; set; }
    }
}