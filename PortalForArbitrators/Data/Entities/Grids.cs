﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalForArbitrators.Data.Entities
{
    public class Grids : IIdentificated
    {
        public Guid Id { get; set; }
        public int BindGrid { get; set; } //bool
        public int BindGide { get; set; } //bool
        public int BindObject { get; set; } //bool
               
        public int ShowGrid { get; set; } //bool
        public int ShowGide { get; set; } //bool
        public int ShowObject { get; set; } //bool

        public int Size { get; set; }
        public string Color { get; set; }
    }
}
