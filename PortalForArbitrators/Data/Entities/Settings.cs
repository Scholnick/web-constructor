﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PortalForArbitrators.Data.Entities
{
    public class Settings : IIdentificated
    {
        public Settings()
        {
            Robots = new RobotsSettings();
            Cookie = new CookieSettings();
        }

        [Key]
        public Guid Id { get; set; } = Guid.NewGuid();
        public int publishNeedHttps { get; set; } //bool
        public int publishNeedRoute { get; set; } //bool
        public int redirectToWww { get; set; } //bool
        public int useTrailingSlash { get; set; } //bool
        public string metatags { get; set; }
        public string script { get; set; }

        #region Robots
        public Guid RobotsId { get; set; }

        public RobotsSettings Robots { get; set; }
        #endregion

        #region Cookie
        public Guid CookieId { get; set; }

        public CookieSettings Cookie { get; set; }
        #endregion
    }


    public class RobotsSettings : IIdentificated
    {
        [Key]
        public Guid Id { get; set; } = Guid.NewGuid();
        public int isEnable { get; set; } //bool
        public string body { get; set; }
    }

    public class CookieSettings : IIdentificated
    {
        [Key]
        public Guid Id { get; set; } = Guid.NewGuid();
        public int isEnable { get; set; } //bool
        public string font { get; set; }
        public string size { get; set; }
        public string message { get; set; }
        public string textButton { get; set; }
        public string background { get; set; }
        public string colorMessage { get; set; }
        public string colorButton { get; set; }
        public string colorTextButton { get; set; }
    }

}
