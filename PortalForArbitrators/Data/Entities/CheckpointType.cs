﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalForArbitrators.Data.Entities
{
    public enum CheckpointType
    {
        official = 1,
        system = 2,
        system_god = 3
    }
}
