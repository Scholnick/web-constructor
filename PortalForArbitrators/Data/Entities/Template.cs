﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalForArbitrators.Data.Entities
{
    public class Template : IIdentificated
    {
        public Guid Id { get; set; }
        public string Key { get; set; }
        public string Icon { get; set; }
        public string Title { get; set; }
        public int IsPopular { get; set; } //bool
    }
}
