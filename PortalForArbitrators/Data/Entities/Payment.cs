﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace PortalForArbitrators.Data.Entities
{
    public class Payment : IIdentificated
    {
        public Guid Id { get; set; }
        [Column(TypeName = "decimal(18,4)")]
        public decimal Sum { get; set; }
        public string ServiceName { get; set; }
        public string Info { get; set; }
        public string Status { get; set; }
        public DateTime DateOfOperation { get; set; }
        public User User { get; set; }
        public string UserId { get; set; }
    }
}