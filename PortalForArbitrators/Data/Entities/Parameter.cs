﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PortalForArbitrators.Data.Entities
{
    public class Parameter : IIdentificated
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public string Value { get; set; }
    }
}