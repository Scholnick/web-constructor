﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalForArbitrators.Data.Entities
{
    public class Geolocation : IIdentificated
    {
        public Guid Id { get; set; }

        /// <summary>
        /// Подпись
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Индекс популярности. null|int. Выставляется для 3-х более попурярных стран
        /// </summary>
        public int pupular_index { get; set; }

        /// <summary>
        /// фото
        /// </summary>
        public string ImageUrl { get; set; }

    }
}
